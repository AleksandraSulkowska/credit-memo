table 50160 "ITV Sales Corr. Line"
{
    Caption = 'Sales Correction Line';
    DrillDownPageID = "ITV Sales Corr. Lines";
    LookupPageID = "ITV Sales Corr. Lines";

    fields
    {
        field(1; "Document Type"; Option)
        {
            Caption = 'Document Type';
            DataClassification = CustomerContent;
            OptionCaption = 'Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order';
            OptionMembers = Quote,Order,Invoice,"Credit Memo","Blanket Order","Return Order";
        }
        field(3; "Document No."; Code[20])
        {
            Caption = 'Document No.';
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = "Sales Header"."No." WHERE("Document Type" = FIELD("Document Type"));
        }
        field(4; "Line No."; Integer)
        {
            Caption = 'Line No.';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(5; Type; Option)
        {
            Caption = 'Type';
            Editable = false;
            DataClassification = CustomerContent;
            OptionCaption = ' ,G/L Account,Item,Resource,Fixed Asset,Charge (Item)';
            OptionMembers = " ","G/L Account",Item,Resource,"Fixed Asset","Charge (Item)";
        }
        field(6; "No."; Code[20])
        {
            Caption = 'No.';
            DataClassification = CustomerContent;
            Editable = false;
            TableRelation = if (Type = CONST(" ")) "Standard Text"
            else
            if (Type = CONST("G/L Account")) "G/L Account"
            else
            if (Type = CONST(Item)) Item
            else
            if (Type = CONST(Resource)) Resource
            else
            if (Type = CONST("Fixed Asset")) "Fixed Asset"
            else
            if (Type = CONST("Charge (Item)")) "Item Charge";
        }
        field(11; Description; Text[100])
        {
            Caption = 'Description';
            DataClassification = CustomerContent;
        }
        field(12; "Description 2"; Text[50])
        {
            Caption = 'Description 2';
            DataClassification = CustomerContent;
        }
        field(13; "Unit of Measure"; Text[50])
        {
            Caption = 'Unit of Measure';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(15; Quantity; Decimal)
        {
            Caption = 'Quantity';
            DecimalPlaces = 0 : 5;
            DataClassification = CustomerContent;

            trigger OnValidate()
            var
                TempSalesLine: Record "Sales Line" temporary;
                LineDiscount: Integer;
            begin
                if Rec.Quantity = xRec.Quantity then
                    exit;
                CheckIfNegative(Quantity, FieldCaption(Quantity));
                InitTempSalesLine(TempSalesLine);
                LineDiscount := TempSalesLine."Line Discount %";
                if SalesCorrReason."Correction Type" in [
                                                         SalesCorrReason."Correction Type"::Quantity,
                                                         SalesCorrReason."Correction Type"::"Quantity and Value",
                                                         SalesCorrReason."Correction Type"::"System (Quantity)",
                                                         SalesCorrReason."Correction Type"::Cancel]
                then begin
                    TempSalesLine.Validate(Quantity);
                    TempSalesLine."Line Discount %" := LineDiscount;
                    TempSalesLine.Validate("Unit Price");
                end else
                    Error(CannotChangeWhenErr, FieldCaption(Quantity), SalesCorrReason.FieldCaption("Correction Type"),
                      SalesCorrReason."Correction Type");
                UpdateSalesCorrLine(TempSalesLine);
            end;
        }
        field(22; "Unit Price"; Decimal)
        {
            AutoFormatExpression = "Currency Code";
            CaptionClass = GetCaptionClass(FieldNo("Unit Price"));
            Caption = 'Unit Price';
            DataClassification = CustomerContent;

            trigger OnValidate()
            var
                TempSalesLine: Record "Sales Line" temporary;
            begin
                if Rec."Unit Price" = xRec."Unit Price" then
                    exit;
                CheckIfNegative(Rec."Unit Price", FieldCaption("Unit Price"));
                InitTempSalesLine(TempSalesLine);
                if SalesCorrReason."Correction Type" in [
                                                         SalesCorrReason."Correction Type"::"Unit Price",
                                                         SalesCorrReason."Correction Type"::"Quantity and Value",
                                                         SalesCorrReason."Correction Type"::"VAT Rate",
                                                         SalesCorrReason."Correction Type"::"System (Quantity)",
                                                         SalesCorrReason."Correction Type"::Cancel]
                then
                    TempSalesLine.Validate("Unit Price")
                else
                    Error(CannotChangeWhenErr, FieldCaption("Unit Price"), SalesCorrReason.FieldCaption("Correction Type"), SalesCorrReason."Correction Type");
                UpdateSalesCorrLine(TempSalesLine);
            end;
        }
        field(25; "VAT %"; Decimal)
        {
            Caption = 'VAT %';
            DecimalPlaces = 0 : 5;
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(27; "Line Discount %"; Decimal)
        {
            Caption = 'Line Discount %';
            DecimalPlaces = 0 : 5;
            MaxValue = 100;
            MinValue = 0;
            DataClassification = CustomerContent;

            trigger OnValidate()
            var
                TempSalesLine: Record "Sales Line" temporary;
            begin
                if Rec."Line Discount %" = xRec."Line Discount %" then
                    exit;
                CheckIfNegative(Rec."Line Discount %", FieldCaption("Line Discount %"));
                InitTempSalesLine(TempSalesLine);
                if SalesCorrReason."Correction Type" in [
                                                         SalesCorrReason."Correction Type"::Discount,
                                                         SalesCorrReason."Correction Type"::Cancel]
                then
                    TempSalesLine.Validate("Line Discount %")
                else
                    Error(CannotChangeWhenErr, FieldCaption("Line Discount %"), SalesCorrReason.FieldCaption("Correction Type"), SalesCorrReason."Correction Type");
                UpdateSalesCorrLine(TempSalesLine);
            end;
        }
        field(28; "Line Discount Amount"; Decimal)
        {
            AutoFormatExpression = "Currency Code";
            AutoFormatType = 1;
            Caption = 'Line Discount Amount';
            DataClassification = CustomerContent;

            trigger OnValidate()
            var
                TempSalesLine: Record "Sales Line" temporary;
            begin
                if Rec."Line Discount Amount" = xRec."Line Discount Amount" then
                    exit;
                CheckIfNegative(Rec."Line Discount Amount", FieldCaption("Line Discount Amount"));
                InitTempSalesLine(TempSalesLine);
                if SalesCorrReason."Correction Type" in [
                                                         SalesCorrReason."Correction Type"::Discount,
                                                         SalesCorrReason."Correction Type"::"Quantity and Value",
                                                         SalesCorrReason."Correction Type"::Cancel]
                then begin
                    if Round(Rec.Quantity * Rec."Unit Price", Currency."Amount Rounding Precision") <> 0 then
                        TempSalesLine."Line Discount %" :=
                          Round(
                            Rec."Line Discount Amount" / Round(Rec.Quantity * Rec."Unit Price", Currency."Amount Rounding Precision") * 100,
                            0.00001)
                    else
                        TempSalesLine."Line Discount %" := 0;
                    TempSalesLine.Validate("Line Discount Amount")
                end else
                    Error(CannotChangeWhenErr, FieldCaption("Line Discount Amount"), SalesCorrReason.FieldCaption("Correction Type"), SalesCorrReason."Correction Type");
                UpdateSalesCorrLine(TempSalesLine);
            end;
        }
        field(29; Amount; Decimal)
        {
            AutoFormatExpression = "Currency Code";
            AutoFormatType = 1;
            Caption = 'Amount';
            Editable = false;
            DataClassification = CustomerContent;

            trigger OnValidate()
            var
                TempSalesLine: Record "Sales Line" temporary;
            begin
                if Rec.Amount = xRec.Amount then
                    exit;
                CheckIfNegative(Rec.Amount, FieldCaption(Amount));
                InitTempSalesLine(TempSalesLine);
                if SalesCorrReason."Correction Type" in [
                                                         SalesCorrReason."Correction Type"::Discount,
                                                         SalesCorrReason."Correction Type"::"Quantity and Value",
                                                         SalesCorrReason."Correction Type"::Cancel]
                then
                    TempSalesLine.Validate(Amount)
                else
                    Error(CannotChangeWhenErr, FieldCaption(Amount), SalesCorrReason.FieldCaption("Correction Type"), SalesCorrReason."Correction Type");
                UpdateSalesCorrLine(TempSalesLine);
            end;
        }
        field(30; "Amount Including VAT"; Decimal)
        {
            AutoFormatExpression = "Currency Code";
            AutoFormatType = 1;
            Caption = 'Amount Including VAT';
            Editable = false;
            DataClassification = CustomerContent;

            trigger OnValidate()
            var
                TempSalesLine: Record "Sales Line" temporary;
            begin
                if Rec."Amount Including VAT" = xRec."Amount Including VAT" then
                    exit;
                CheckIfNegative(Rec."Amount Including VAT", FieldCaption("Amount Including VAT"));
                InitTempSalesLine(TempSalesLine);
                if SalesCorrReason."Correction Type" in [
                                                         SalesCorrReason."Correction Type"::Discount,
                                                         SalesCorrReason."Correction Type"::"Quantity and Value",
                                                         SalesCorrReason."Correction Type"::Cancel]
                then
                    TempSalesLine.Validate("Amount Including VAT")
                else
                    Error(CannotChangeWhenErr, FieldCaption("Amount Including VAT"), SalesCorrReason.FieldCaption("Correction Type"), SalesCorrReason."Correction Type");
                UpdateSalesCorrLine(TempSalesLine);
            end;
        }
        field(69; "Inv. Discount Amount"; Decimal)
        {
            AutoFormatExpression = "Currency Code";
            AutoFormatType = 1;
            Caption = 'Inv. Discount Amount';
            DataClassification = CustomerContent;

            trigger OnValidate()
            var
                TempSalesLine: Record "Sales Line" temporary;
            begin
                if Rec."Inv. Discount Amount" = xRec."Inv. Discount Amount" then
                    exit;
                CheckIfNegative(Rec."Inv. Discount Amount", FieldCaption("Inv. Discount Amount"));
                InitTempSalesLine(TempSalesLine);
                if not InvoiceDiscAllowed(SalesCorrReason."Correction Type") then
                    Error(ChangeNotAllowedErr, FieldCaption("Inv. Discount Amount"));
                if SalesCorrReason."Correction Type" in [
                                                         SalesCorrReason."Correction Type"::Discount,
                                                         SalesCorrReason."Correction Type"::"Quantity and Value",
                                                         SalesCorrReason."Correction Type"::Cancel]
                then
                    TempSalesLine.Validate("Inv. Discount Amount")
                else
                    Error(CannotChangeWhenErr, FieldCaption("Inv. Discount Amount"), SalesCorrReason.FieldCaption("Correction Type"), SalesCorrReason."Correction Type");
                UpdateSalesCorrLine(TempSalesLine);
            end;
        }
        field(77; "VAT Calculation Type"; Option)
        {
            Caption = 'VAT Calculation Type';
            DataClassification = CustomerContent;
            OptionCaption = 'Normal VAT,Reverse Charge VAT,Full VAT,Sales Tax';
            OptionMembers = "Normal VAT","Reverse Charge VAT","Full VAT","Sales Tax";
        }
        field(80; "Attached to Line No."; Integer)
        {
            Caption = 'Attached to Line No.';
            Editable = false;
            TableRelation = "Sales Line"."Line No." WHERE("Document Type" = FIELD("Document Type"),
                                                           "Document No." = FIELD("Document No."));
            DataClassification = CustomerContent;
        }
        field(89; "VAT Bus. Posting Group"; Code[20])
        {
            Caption = 'VAT Bus. Posting Group';
            TableRelation = "VAT Business Posting Group";
            DataClassification = CustomerContent;
        }
        field(90; "VAT Prod. Posting Group"; Code[20])
        {
            Caption = 'VAT Prod. Posting Group';
            TableRelation = "VAT Product Posting Group";
            DataClassification = CustomerContent;
            trigger OnValidate()
            var
                TempSalesLine: Record "Sales Line" temporary;
            begin
                if Rec."VAT Prod. Posting Group" = xRec."VAT Prod. Posting Group" then
                    exit;
                InitTempSalesLine(TempSalesLine);
                if SalesCorrReason."Correction Type" = SalesCorrReason."Correction Type"::"VAT Rate" then
                    TempSalesLine.Validate("VAT Prod. Posting Group")
                else
                    Error(CannotChangeWhenErr, FieldCaption("VAT Prod. Posting Group"), SalesCorrReason.FieldCaption("Correction Type"), SalesCorrReason."Correction Type");
                UpdateSalesCorrLine(TempSalesLine);
            end;
        }
        field(91; "Currency Code"; Code[10])
        {
            Caption = 'Currency Code';
            Editable = false;
            TableRelation = Currency;
            DataClassification = CustomerContent;
        }
        field(99; "VAT Base Amount"; Decimal)
        {
            AutoFormatExpression = "Currency Code";
            AutoFormatType = 1;
            Caption = 'VAT Base Amount';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(103; "Line Amount"; Decimal)
        {
            AutoFormatExpression = "Currency Code";
            AutoFormatType = 1;
            CaptionClass = GetCaptionClass(FieldNo("Line Amount"));
            DataClassification = CustomerContent;
            Caption = 'Line Amount';

            trigger OnValidate()
            var
                TempSalesLine: Record "Sales Line" temporary;
            begin
                if Rec."Line Amount" = xRec."Line Amount" then
                    exit;
                CheckIfNegative("Line Amount", FieldCaption("Line Amount"));
                InitTempSalesLine(TempSalesLine);
                if SalesCorrReason."Correction Type" in [
                                                         SalesCorrReason."Correction Type"::Discount,
                                                         SalesCorrReason."Correction Type"::"Quantity and Value",
                                                         SalesCorrReason."Correction Type"::Cancel]
                then
                    TempSalesLine.Validate("Line Amount")
                else
                    Error(CannotChangeWhenErr, FieldCaption("Line Amount"), SalesCorrReason.FieldCaption("Correction Type"), SalesCorrReason."Correction Type");
                UpdateSalesCorrLine(TempSalesLine);
            end;
        }
        field(105; "Inv. Disc. Amount to Invoice"; Decimal)
        {
            AutoFormatExpression = "Currency Code";
            AutoFormatType = 1;
            Caption = 'Inv. Disc. Amount to Invoice';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(106; "VAT Identifier"; Code[20])
        {
            Caption = 'VAT Identifier';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(1002; "Job Contract Entry No."; Integer)
        {
            Caption = 'Job Contract Entry No.';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(5404; "Qty. per Unit of Measure"; Decimal)
        {
            Caption = 'Qty. per Unit of Measure';
            DecimalPlaces = 0 : 5;
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(5407; "Unit of Measure Code"; Code[10])
        {
            Caption = 'Unit of Measure Code';
            Editable = false;
            TableRelation = if (Type = CONST(Item)) "Item Unit of Measure".Code WHERE("Item No." = FIELD("No."))
            else
            "Unit of Measure";
            DataClassification = CustomerContent;
        }
        field(5415; "Quantity (Base)"; Decimal)
        {
            Caption = 'Quantity (Base)';
            DecimalPlaces = 0 : 5;
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(52063067; "Corr. Invoice No."; Code[20])
        {
            Caption = 'Corr. Invoice No.';
            Editable = false;
            TableRelation = "Sales Invoice Line"."Document No.";
            DataClassification = CustomerContent;
        }

        field(52063069; "Corr. Invoice Sales Date"; Date)
        {
            Caption = 'Corr. Invoice Sales Date';
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(52063070; "Corr. Invoice Line No."; Integer)
        {
            Caption = 'Corr. Invoice Line No.';
            TableRelation = "Sales Invoice Line"."Line No.";
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(52063071; "Before/ After correction"; Enum "ITV Before/After Line Type")
        {
            Caption = 'Before/ After correction';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(52063072; "Correction Line No."; Integer)
        {
            Caption = 'Correction Line No.';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(52063073; "Consecutive No."; Integer)
        {
            Caption = 'Consecutive No.';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(52063074; "Inconsistent Line"; Boolean)
        {
            Caption = 'Inconsistent Line';
            Editable = false;
            DataClassification = CustomerContent;
        }
    }

    keys
    {
        key(Key1; "Document Type", "Document No.", "Correction Line No.", "Before/ After correction")
        {
            MaintainSIFTIndex = false;
        }
    }

    fieldgroups
    {
    }

    trigger OnDelete()
    begin
        DeleteOppositeCorrectionLine();
    end;

    trigger OnInsert()
    begin
        InsertLineAfter();
    end;

    trigger OnModify()
    begin
        TestField(Rec."Before/ After correction", "Before/ After correction"::After);
    end;

    trigger OnRename()
    begin
        Error(CannotRenameErr, TableCaption);
    end;

    var
        SalesCorrReason: Record "ITV Sales Correction Reason";
        SalesHeader: Record "Sales Header";
        Currency: Record Currency;
        CannotRenameErr: Label 'You cannot rename a %1.', Comment = '%1 = Table name';
        CannotChangeWhenErr: Label 'You can not change %1 when %2 is %3.', Comment = '%1 = Field to change name ; %2 = Correction type field name ; %3 = Correction type';
        ValueCannotBeNegativeErr: Label 'Value in field %1 cannot be negative.', Comment = '%1 = Field name';
        ChangeNotAllowedErr: Label '%1 change is not allowed.', Comment = '%1 = Field name';

    local procedure CheckIfCancelReason(SalesHeader: Record "Sales Header"): Boolean
    var
        SalesCorrectionReason: Record "ITV Sales Correction Reason";
    begin
        SalesCorrectionReason.SetRange(Code, SalesHeader."ITI Correction Reason");
        SalesCorrectionReason.FindFirst();
        exit((SalesCorrectionReason."Correction Type" = SalesCorrectionReason."Correction Type"::Cancel) or
             (SalesCorrectionReason."Correction Type" = SalesCorrectionReason."Correction Type"::"System (Cancel)"));
    end;

    local procedure DeleteOppositeCorrectionLine()
    var
        SalesLine: Record "Sales Line";
        SalesCorrLine: Record "ITV Sales Corr. Line";
    begin
        TestStatusOpen();
        SalesLine.SetRange("Document Type", Rec."Document Type");
        SalesLine.SetRange("Document No.", Rec."Document No.");
        SalesLine.SetRange("ITV Correction Line No.", Rec."Correction Line No.");
        if not SalesLine.IsEmpty then begin
            SalesLine.ModifyAll("ITI Corr. Invoice No.", '');
            SalesLine.ModifyAll("ITI Corr. Invoice Sales Date", 0D);
            SalesLine.ModifyAll("ITI Corr. Invoice Line No.", 0);
            SalesLine.ModifyAll("ITV Correction Line No.", 0);
        end;

        case Rec."Before/ After correction" of
            Rec."Before/ After correction"::Before:
                if SalesCorrLine.Get(Rec."Document Type", Rec."Document No.", Rec."Correction Line No.", Rec."Before/ After correction"::After) then
                    SalesCorrLine.Delete();
            Rec."Before/ After correction"::After:
                if SalesCorrLine.Get(Rec."Document Type", Rec."Document No.", Rec."Correction Line No.", Rec."Before/ After correction"::Before) then
                    SalesCorrLine.Delete();
        end;
    end;

    local procedure InsertLineAfter()
    var
        SalesCorrLine: Record "ITV Sales Corr. Line";
    begin
        TestField(Rec."Before/ After correction", Rec."Before/ After correction"::Before);
        TestStatusOpen();
        SalesCorrLine.SetRange("Document Type", Rec."Document Type");
        SalesCorrLine.SetRange("Document No.", Rec."Document No.");
        SalesCorrLine.SetFilter("Correction Line No.", '<>0');
        if SalesCorrLine.FindLast() then
            Rec."Correction Line No." := SalesCorrLine."Correction Line No." + 1
        else
            Rec."Correction Line No." := 1;
        SalesCorrLine.Init();
        SalesCorrLine.TransferFields(Rec);
        SalesCorrLine."Before/ After correction" := Rec."Before/ After correction"::After;
        GetSalesHeader();
        if CheckIfCancelReason(SalesHeader) then begin
            SalesCorrLine.UpdateLineForCancelReason();
        end;
        SalesCorrLine.Insert();
    end;

    procedure GetCaptionClass(FieldNumber: Integer): Text[80]
    var
        TempSalesLine: Record "Sales Line" temporary;
        SalesLineCaptionClassMgmt: Codeunit "Sales Line CaptionClass Mgmt";
    begin
        TempSalesLine.Init();
        TempSalesLine.TransferFields(Rec);
        exit(SalesLineCaptionClassMgmt.GetSalesLineCaptionClass(TempSalesLine, FieldNumber));
    end;

    procedure TestStatusOpen()
    begin
        GetSalesHeader();
        SalesHeader.TestField(Status, SalesHeader.Status::Open);
    end;

    procedure CopyFromSalesInvoiceLine(SalesInvoiceLine: Record "Sales Invoice Line"; SalesInvoiceHeader: Record "Sales Invoice Header"; ToSalesHeader: Record "Sales Header")
    var
        PostedSalesCorrLine: Record "ITV Posted Sales Corr. Line";
    begin
        Rec.TransferFields(SalesInvoiceLine);
        PostedSalesCorrLine.Reset();
        PostedSalesCorrLine.SetCurrentKey("Corr. Invoice No.", "Corr. Invoice Line No.", "Consecutive No.");
        PostedSalesCorrLine.SetRange("Corr. Invoice No.", SalesInvoiceLine."Document No.");
        PostedSalesCorrLine.SetRange("Corr. Invoice Line No.", SalesInvoiceLine."Line No.");
        PostedSalesCorrLine.SetRange("Before/ After correction", PostedSalesCorrLine."Before/ After correction"::After);
        if PostedSalesCorrLine.FindLast() and
           (PostedSalesCorrLine.Quantity <> 0)
        then begin
            Rec."Consecutive No." := 1;
            Rec.Quantity := PostedSalesCorrLine.Quantity;
            Rec."Unit Price" := PostedSalesCorrLine."Unit Price";
            Rec."Line Discount %" := PostedSalesCorrLine."Line Discount %";
            Rec."Line Discount Amount" := PostedSalesCorrLine."Line Discount Amount";
            Rec."Inv. Discount Amount" := PostedSalesCorrLine."Inv. Discount Amount";
            Rec."VAT Prod. Posting Group" := PostedSalesCorrLine."VAT Prod. Posting Group";
            Rec."VAT Bus. Posting Group" := PostedSalesCorrLine."VAT Bus. Posting Group";
            Rec."VAT %" := PostedSalesCorrLine."VAT %";
            Rec."Amount Including VAT" := PostedSalesCorrLine."Amount Including VAT";
            Rec."VAT Identifier" := PostedSalesCorrLine."VAT Identifier";
            Rec.Amount := PostedSalesCorrLine.Amount;
            Rec."VAT Base Amount" := PostedSalesCorrLine."VAT Base Amount";
            Rec."Line Amount" := PostedSalesCorrLine."Line Amount";
            Rec."Quantity (Base)" := PostedSalesCorrLine."Quantity (Base)";
            Rec.Description := PostedSalesCorrLine.Description;
            Rec."Description 2" := PostedSalesCorrLine."Description 2";
            Rec."Consecutive No." := PostedSalesCorrLine."Consecutive No." + 1;
            Rec."Document Type" := ToSalesHeader."Document Type";
            Rec."Document No." := ToSalesHeader."No.";
            Rec."Corr. Invoice No." := SalesInvoiceLine."Document No.";
            Rec."Corr. Invoice Line No." := SalesInvoiceLine."Line No.";
            Rec."Corr. Invoice Sales Date" := GetSalesInvheaderSalesDate(SalesInvoiceHeader);
            Rec."Inconsistent Line" := true;
        end else begin
            Rec."Consecutive No." := 1;
            Rec."Document Type" := ToSalesHeader."Document Type";
            Rec."Document No." := ToSalesHeader."No.";
            Rec."Corr. Invoice No." := SalesInvoiceLine."Document No.";
            Rec."Corr. Invoice Line No." := SalesInvoiceLine."Line No.";
            Rec."Corr. Invoice Sales Date" := GetSalesInvheaderSalesDate(SalesInvoiceHeader);
            Rec."Inconsistent Line" := true;
        end;

    end;

    local procedure GetSalesInvHeaderSalesDate(SalesInvoiceHeader: Record "Sales Invoice Header"): Date

    begin
        if SalesInvoiceHeader."ITI Sales Date" = 0D then
            exit(SalesInvoiceHeader."Posting Date")
        else
            exit(SalesInvoiceHeader."ITI Sales Date");
    end;

    local procedure InitTempSalesLine(var TempSalesLine: Record "Sales Line" temporary)
    var
        SalesInvoiceLine: Record "Sales Invoice Line";
    begin
        GetSalesHeader();
        GetSalesCorrReason();
        GetCurrency();
        TempSalesLine.Init();
        SalesInvoiceLine.Get("Corr. Invoice No.", "Corr. Invoice Line No.");
        TempSalesLine.TransferFields(SalesInvoiceLine);
        TempSalesLine.TransferFields(Rec);
        TempSalesLine."Job Contract Entry No." := 0;
        TempSalesLine."Shipment No." := '';
    end;

    local procedure UpdateSalesCorrLine(var TempSalesLine: Record "Sales Line" temporary)
    begin
        Rec.TransferFields(TempSalesLine);
        Rec."Inconsistent Line" := true;
        Rec.Modify();
    end;

    local procedure GetSalesHeader()
    begin
        TestField(Rec."Document No.");
        if (Rec."Document Type" <> SalesHeader."Document Type") or (Rec."Document No." <> SalesHeader."No.") then
            SalesHeader.Get("Document Type", "Document No.");
    end;

    local procedure GetSalesCorrReason()
    begin
        SalesHeader.TestField("ITI Correction Reason");
        if SalesHeader."ITI Correction Reason" <> SalesCorrReason.Code then
            SalesCorrReason.Get(SalesHeader."ITI Correction Reason");
    end;

    local procedure GetCurrency()
    begin
        if SalesHeader."Currency Code" = '' then
            Currency.InitRoundingPrecision()
        else begin
            Currency.Get(SalesHeader."Currency Code");
            Currency.TestField("Amount Rounding Precision");
        end;
    end;

    procedure RecreateSalesCorrLines(SalesHeader: Record "Sales Header")
    var
        ITVRecreateSalesCorrLinesCodeunit: Codeunit ITVRecreateSalesCorrLines;
    begin
        ITVRecreateSalesCorrLinesCodeunit.RecreateSalesCorrLines(SalesHeader);
    end;

    procedure UpdateLineForCancelReason()
    begin
        Rec.Quantity := 0;
        Rec."Unit Price" := 0;
        Rec."Line Discount %" := 0;
        Rec."Line Discount Amount" := 0;
        Rec.Amount := 0;
        Rec."Amount Including VAT" := 0;
        Rec."Inv. Discount Amount" := 0;
        Rec."VAT Base Amount" := 0;
        Rec."Line Amount" := 0;
        Rec."Quantity (Base)" := 0;
    end;

    local procedure CheckIfNegative(Value: Decimal; TheFieldCaption: Text[250])
    begin
        if Value < 0 then
            Error(ValueCannotBeNegativeErr, TheFieldCaption);
    end;

    procedure InvoiceDiscAllowed(var CorrectionType: Enum "ITV Correction Reason Type"): Boolean
    var
        SalesCorrLineBefore: Record "ITV Sales Corr. Line";
    begin
        if Rec."Before/ After correction" <> "Before/ After correction"::After then
            exit(false);
        if Type <> Type::Item then
            exit(false);
        SalesCorrLineBefore.Get("Document Type", "Document No.", "Correction Line No.", "Before/ After correction"::Before);
        if (CorrectionType = CorrectionType::"Quantity and Value") and
           (SalesCorrLineBefore."Quantity (Base)" - "Quantity (Base)" = 0)
        then
            exit(true)
        else
            exit(false);
    end;

}

