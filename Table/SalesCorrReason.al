table 50162 "ITV Sales Correction Reason"
{
    Caption = 'Sales Correction Reason';
    LookupPageID = "ITV Sales Correction Reasons";

    fields
    {
        field(1; "Code"; Code[10])
        {
            Caption = 'Code';
            DataClassification = CustomerContent;
            NotBlank = true;
        }
        field(2; Description; Text[50])
        {
            Caption = 'Description';
            DataClassification = CustomerContent;
        }
        field(3; "Correction Type"; Enum "ITV Correction Reason Type")
        {
            Caption = 'Correction Type';
            DataClassification = CustomerContent;
            InitValue = " ";
        }
        field(4; "Item Charge Code"; Code[20])
        {
            Caption = 'Item Charge Code';
            DataClassification = CustomerContent;
            TableRelation = "Item Charge";
        }
    }

    keys
    {
        key(Key1; "Code")
        {
        }
    }

    fieldgroups
    {
    }

    var
        SystemCorrReasonDeleteErr: Label 'You can not delete System Correction Reasons.';
        SystemCorrReasonModifyErr: Label 'You can not modify System Correction Reasons';
        MoreThanOneSystemCorrReasonErr: Label 'You can not insert more than One %1 %2', Comment = '%1 - Correction Type Field Caption, %2 - Correction Type Value';

    trigger OnDelete()
    begin
        if Rec."Correction Type" IN [Rec."Correction Type"::"System (Cancel)", Rec."Correction Type"::"System (Quantity)"] then
            Error(SystemCorrReasonDeleteErr);
    end;

    trigger OnModify()
    begin
        if ((xRec."Correction Type" = Rec."Correction Type"::"System (Cancel)") and (Rec."Correction Type" <> Rec."Correction Type"::"System (Cancel)")) or
           ((xRec."Correction Type" = Rec."Correction Type"::"System (Quantity)") and (Rec."Correction Type" <> Rec."Correction Type"::"System (Quantity)")) then
            Error(SystemCorrReasonModifyErr);

        CheckSystemReasonExist(Rec."Correction Type");
    end;

    trigger OnInsert()

    begin
        CheckSystemReasonExist(Rec."Correction Type");
    end;

    local procedure CheckSystemReasonExist(CorrectionType: Enum "ITV Correction Reason Type")
    var
        SalesCorrectionReason: Record "ITV Sales Correction Reason";
    begin
        if not (CorrectionType in [CorrectionType::"System (Cancel)", CorrectionType::"System (Quantity)"]) then
            exit;

        SalesCorrectionReason.SetRange(SalesCorrectionReason."Correction Type", CorrectionType);
        SalesCorrectionReason.SetFilter(Code, '<>%1', Rec.Code);
        if not SalesCorrectionReason.IsEmpty then
            Error(MoreThanOneSystemCorrReasonErr, Rec.FieldCaption("Correction Type"), Rec."Correction Type");
    end;

    procedure GetSystemQuantityReason()
    begin
        GetReason("Correction Type"::"System (Quantity)");
    end;

    procedure GetSystemCancelReason()
    begin
        GetReason("Correction Type"::"System (Cancel)");
    end;

    local procedure GetReason(CorrectionReasonType: Enum "ITV Correction Reason Type")
    begin
        SetRange("Correction Type", CorrectionReasonType);
        FindFirst();
    end;

}