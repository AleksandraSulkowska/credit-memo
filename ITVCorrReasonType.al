enum 50161 "ITV Correction Reason Type"
{
    value(0; Quantity) { Caption = 'Quantity'; }
    value(1; "Unit Price") { Caption = 'Unit Price'; }
    value(2; Discount) { Caption = 'Discount'; }
    value(3; "VAT Rate") { Caption = 'VAT Rate'; }
    value(4; "Quantity and Value") { Caption = 'Quantity and Value'; }
    value(5; Cancel) { Caption = 'Cancel'; }
    value(6; "System (Quantity)") { Caption = 'System (Quantity)'; }
    value(7; "System (Cancel)") { Caption = 'System (Cancel)'; }
    value(8; " ") { Caption = ' '; }

}