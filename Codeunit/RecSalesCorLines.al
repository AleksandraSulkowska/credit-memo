codeunit 50160 ITVRecreateSalesCorrLines
{
    procedure RecreateSalesCorrLines(SalesHeader: Record "Sales Header")
    var
        SalesCorrLine: Record "ITV Sales Corr. Line";
        TempSalesCorrLine: Record "ITV Sales Corr. Line" temporary;
    begin
        SetSalesCorrLineFilter(SalesCorrLine, SalesHeader);
        IF SalesCorrLineIsEmpty(SalesCorrLine) then
            exit;
        CopySalesCorrLinesToBuffer(SalesCorrLine, TempSalesCorrLine);
        DeleteSalesCorrLines(SalesCorrLine);
        CreateSalesCorrLinesFromBuffer(TempSalesCorrLine);
    end;

    local procedure SetSalesCorrLineFilter(var SalesCorrLine: Record "ITV Sales Corr. Line"; SalesHeader: Record "Sales Header")
    begin
        SalesCorrLine.SetRange("Document Type", SalesHeader."Document Type");
        SalesCorrLine.SetRange("Document No.", SalesHeader."No.");
    end;

    local procedure SalesCorrLineIsEmpty(var SalesCorrLine: Record "ITV Sales Corr. Line"): Boolean
    begin
        exit(SalesCorrLine.IsEmpty);
    end;

    local procedure CopySalesCorrLinesToBuffer(var SalesCorrLine: Record "ITV Sales Corr. Line"; var TempSalesCorrLine: Record "ITV Sales Corr. Line" temporary)
    begin
        TempSalesCorrLine.DeleteAll();
        SalesCorrLine.SetRange("Before/ After correction", SalesCorrLine."Before/ After correction"::Before);
        if SalesCorrLine.FindSet() then
            repeat
                TempSalesCorrLine := SalesCorrLine;
                TempSalesCorrLine.Insert();
            until SalesCorrLine.Next() = 0;
        SalesCorrLine.SetRange("Before/ After correction");
    end;

    local procedure DeleteSalesCorrLines(var SalesCorrLine: Record "ITV Sales Corr. Line")
    begin
        SalesCorrLine.DeleteAll(true);
    end;

    local procedure CreateSalesCorrLinesFromBuffer(var TempSalesCorrLine: Record "ITV Sales Corr. Line" temporary)
    var
        SalesCorrLine: Record "ITV Sales Corr. Line";
    begin
        if not TempSalesCorrLine.FindSet() then
            exit;

        repeat
            SalesCorrLine.Init();
            SalesCorrLine := TempSalesCorrLine;
            SalesCorrLine.Insert(true);
        until TempSalesCorrLine.Next() = 0;

        TempSalesCorrLine.DeleteAll();
    end;

    procedure GetInvoiceLinesForCorr(SalesHeader: Record "Sales Header")
    var
        GetSalesInvoiceLines: Page "ITV Get Sales Invoice Lines";
    begin
        SalesHeader.TestField("ITI Correction Reason");
        GetSalesInvoiceLines.SetLinesView(SalesHeader);
        GetSalesInvoiceLines.LookupMode := true;
        if GetSalesInvoiceLines.RunModal() <> ACTION::Cancel then;
    end;
}