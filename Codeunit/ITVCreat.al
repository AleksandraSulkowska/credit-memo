codeunit 50164 "ITVCreateSalesCrMemoLines"
{
    var
        SalesCorrectionReason: Record "ITI Sales Correction Reason";
        SalesReceivablesSetup: Record "Sales & Receivables Setup";
        NoLinesToProcessErr: Label 'No lines to process.';
        DeleteExistingLinesQst: Label 'All existing lines in %1 No. %2 will be deleted.\Do you want to continue?', Comment = '%1 = Document type ; %2 = Document number';
        CreateLinesProgressMsg: Label 'Total Correction Lines   #1########\Checking corr. lines #2########\Creating temporary lines #3########\Inserting sales lines    #4########\Inserting Item Tracking lines  #5########';
        SameValueBeforeAndAfterErr: Label 'Value of %1 field, must be the same in line Before and After (Line %2). ', Comment = '%1 = Field name ; %2 = Correction line number';
        AfterCannotBeGreaterErr: Label '%1 in line After can not be greater than %2 (Line %3).', Comment = '%1 = Field name ; %2 = Quantity ; %3 = Line number';
        YouCanChangeOnlyErr: Label 'You can change only %1 for %2 %3.', Comment = '%1 = Correction type ; %2 = Sales correction table name ; %3 = Correction reason code';
        CannotChangeVATPctErr: Label 'You cannot change VAT % when %1 is %2.', Comment = '%1 = Sales correction table name ; %2 = Correction reason different then VAT';
        CannotChangeAfterWhenCancelErr: Label 'You cannot change line After for correction type Cancellation.';
        CannotCreateCorrWhenCancelErr: Label 'You cannot create correction type Cancel, because one or more credit memos related to Posted Sales Invoice No. %1 exists. Use different Correction Type.', Comment = '%1 = Posted Sales Invoice number';
        Window: Dialog;
        LastLineNo: Integer;
        QtyDiffCannotBeNegativeErr: Label 'Before/ After Quantity difference must not be negative (Item No. %1,Correction Line No. %2).', Comment = '%1 = Item number ; %2 = Correction line number';
        LinesCreated: Integer;
        SalesCrMemoLinesCreatedMsg: Label '%1 Sales Credit Memo Lines from %2 Sales Correction Lines has been created.', Comment = '%1 = How many Sales Credit Memi Lines ; %2 = From how many correction lines';
        SalesSetupRead: Boolean;

    procedure CreateCrMemoLines(SalesHeader: Record "Sales Header")
    var
        TempSalesLine: Record "Sales Line" temporary;
        TotalLines: Integer;
    begin
        InitProgressWindow(SalesHeader, TotalLines);
        CheckPreconditions(SalesHeader);
        DeleteExistingLinesWhenConfirmed(SalesHeader);
        GetSalesSetup();
        CreateTempSalesLines(SalesHeader, TempSalesLine, SalesReceivablesSetup."Exact Cost Reversing Mandatory");
        InsertSalesLines(TempSalesLine);
        CreateItemChargeAssignmentLines(SalesHeader, TempSalesLine);
        CreateItemTrackingLines(TempSalesLine, SalesReceivablesSetup."Exact Cost Reversing Mandatory");
        UpdateSalesDate(TempSalesLine, SalesHeader);
        UpdateCurrencyFactor(TempSalesLine, SalesHeader);
        Finish(TempSalesLine, TotalLines);
    end;

    local procedure CheckPreconditions(SalesHeader: Record "Sales Header")
    var
        TempErrorMessage: Record "Error Message" temporary;
    begin
        TempErrorMessage.ClearLog();
        CheckIfSalesCorrLineEmpty(SalesHeader, TempErrorMessage);
        CheckSalesSetup(TempErrorMessage);
        CheckSalesHeader(SalesHeader, TempErrorMessage);
        CheckSalesCorrectionReason(SalesHeader, TempErrorMessage);
        CheckCorrLines(SalesHeader, TempErrorMessage);
        StopIfErrors(TempErrorMessage);
    end;

    local procedure CheckIfSalesCorrLineEmpty(SalesHeader: Record "Sales Header"; var TempErrorMessage: Record "Error Message" temporary)
    var
        SalesCorrLine: Record "ITV Sales Corr. Line";
    begin
        SalesCorrLine.SetRange("Document Type", SalesHeader."Document Type");
        SalesCorrLine.SetRange("Document No.", SalesHeader."No.");
        if SalesCorrLine.IsEmpty then
            TempErrorMessage.LogSimpleMessage(
                TempErrorMessage."Message Type"::Error,
                NoLinesToProcessErr);
    end;

    local procedure CheckSalesSetup(var TempErrorMessage: Record "Error Message" temporary)
    begin
        GetSalesSetup();
        TempErrorMessage.LogIfNotEqualTo(
            SalesReceivablesSetup,
            SalesReceivablesSetup.FieldNo("Exact Cost Reversing Mandatory"),
            TempErrorMessage."Message Type"::Error,
            true);
    end;

    local procedure CheckSalesHeader(SalesHeader: Record "Sales Header"; var TempErrorMessage: Record "Error Message" temporary)
    begin
        TempErrorMessage.LogIfNotEqualTo(
            SalesHeader,
            SalesHeader.FieldNo(Status),
            TempErrorMessage."Message Type"::Error,
            SalesHeader.Status::Open);

        TempErrorMessage.LogIfEmpty(
            SalesHeader,
            SalesHeader.FieldNo("ITI Correction Reason"),
            TempErrorMessage."Message Type"::Error);

    end;

    local procedure CheckSalesCorrectionReason(SalesHeader: Record "Sales Header"; var TempErrorMessage: Record "Error Message" temporary)
    begin
        GetSalesCorrReason(SalesHeader."ITI Correction Reason");
        if not (SalesCorrectionReason."Correction Type" in [SalesCorrectionReason."Correction Type"::"Quantity and Value",
                                                            SalesCorrectionReason."Correction Type"::Discount,
                                                            SalesCorrectionReason."Correction Type"::"Unit Price"])
        then
            exit;

        TempErrorMessage.LogIfEmpty(
            SalesCorrectionReason,
            SalesCorrectionReason.FieldNo("Item Charge Code"),
            TempErrorMessage."Message Type"::Error);
    end;

    local procedure CheckCorrLines(SalesHeader: Record "Sales Header"; var TempErrorMessage: Record "Error Message" temporary)
    var
        SalesCorrLineBefore: Record "ITV Sales Corr. Line";
        LinesCounter: Integer;
    begin
        GetSalesCorrReason(SalesHeader."ITI Correction Reason");
        SalesCorrLineBefore.SetRange("Document Type", SalesHeader."Document Type");
        SalesCorrLineBefore.SetRange("Document No.", SalesHeader."No.");
        SalesCorrLineBefore.SetRange("Before/ After correction", SalesCorrLineBefore."Before/ After correction"::Before);
        if SalesCorrLineBefore.FindSet() then
            repeat
                LinesCounter := LinesCounter + 1;
                Window.Update(2, LinesCounter);
                CheckCorrLineAgainstCorrReason(SalesHeader, SalesCorrLineBefore, TempErrorMessage);
            until (SalesCorrLineBefore.Next() = 0);
    end;

    local procedure DeleteExistingLinesWhenConfirmed(SalesHeader: Record "Sales Header")
    var
        SalesLine: Record "Sales Line";
    begin
        SalesLine.SetRange("Document Type", SalesHeader."Document Type");
        SalesLine.SetRange("Document No.", SalesHeader."No.");
        if not SalesLine.IsEmpty then
            if Confirm(DeleteExistingLinesQst, false, SalesHeader.FieldCaption("Document Type"), SalesHeader."No.") then
                SalesLine.DeleteAll(true)
            else
                Error('');
    end;

    local procedure InitProgressWindow(SalesHeader: Record "Sales Header"; var TotalLines: Integer)
    var
        SalesCorrLine: Record "ITV Sales Corr. Line";
    begin
        SalesCorrLine.SetRange("Document Type", SalesHeader."Document Type");
        SalesCorrLine.SetRange("Document No.", SalesHeader."No.");
        SalesCorrLine.SetRange("Before/ After correction", SalesCorrLine."Before/ After correction"::Before);
        TotalLines := SalesCorrLine.Count;
        Window.Open(CreateLinesProgressMsg);
        Window.Update(1, TotalLines);
    end;

    local procedure GetSalesSetup()
    begin
        if not SalesSetupRead then
            SalesReceivablesSetup.Get();
        SalesSetupRead := true;
    end;

    local procedure CreateTempSalesLines(SalesHeader: Record "Sales Header"; var TempSalesLine: Record "Sales Line" temporary; ExactCostReverseMandatory: Boolean)
    var
        SalesCorrLineBefore: Record "ITV Sales Corr. Line";
        SalesCorrLineAfter: Record "ITV Sales Corr. Line";
        LinesCounter: Integer;
        JobContractEntryNo: Integer;
    begin
        TempSalesLine.Reset();
        TempSalesLine.DeleteAll();
        GetSalesCorrReason(SalesHeader."ITI Correction Reason");
        SalesCorrLineBefore.SetRange("Document Type", SalesHeader."Document Type");
        SalesCorrLineBefore.SetRange("Document No.", SalesHeader."No.");
        SalesCorrLineBefore.SetRange("Before/ After correction", SalesCorrLineBefore."Before/ After correction"::Before);
        if SalesCorrLineBefore.FindSet() then
            repeat
                LinesCounter := LinesCounter + 1;
                Window.Update(3, LinesCounter);
                SalesCorrLineAfter.Get(SalesCorrLineBefore."Document Type", SalesCorrLineBefore."Document No.", SalesCorrLineBefore."Correction Line No.", SalesCorrLineBefore."Before/ After correction"::After);
                JobContractEntryNo := SalesCorrLineBefore."Job Contract Entry No.";
                SalesCorrLineAfter."Job Contract Entry No." := 0;
                SalesCorrLineBefore."Job Contract Entry No." := 0;
                if SalesCorrectionReason."Correction Type" = SalesCorrectionReason."Correction Type"::"VAT Rate" then
                    InsertVATRateCrMemoLines(SalesHeader, SalesCorrLineBefore, SalesCorrLineAfter, TempSalesLine)
                else
                    case SalesCorrLineAfter.Type of
                        SalesCorrLineAfter.Type::"G/L Account":
                            InsertGLAccQtyOrValueCrMemoLine(SalesHeader, SalesCorrLineBefore, SalesCorrLineAfter, TempSalesLine);
                        SalesCorrLineAfter.Type::Item:
                            if SalesCorrectionReason."Correction Type" <> SalesCorrectionReason."Correction Type"::Quantity then
                                InsertItemQtyValueCrMemoLines(SalesHeader, SalesCorrLineBefore, SalesCorrLineAfter, SalesCorrectionReason, TempSalesLine, ExactCostReverseMandatory)
                            else
                                InsertItemQtyCrMemoLine(SalesHeader, SalesCorrLineBefore, SalesCorrLineAfter, TempSalesLine, ExactCostReverseMandatory);
                        SalesCorrLineAfter.Type::Resource:
                            InsertResourceQtyValueCrMemoLine(SalesHeader, SalesCorrLineBefore, SalesCorrLineAfter, TempSalesLine);
                        SalesCorrLineAfter.Type::"Fixed Asset":
                            InsertFixedAssetQtyValueCrMemoLine(SalesHeader, SalesCorrLineBefore, SalesCorrLineAfter, TempSalesLine);
                        SalesCorrLineAfter.Type::"Charge (Item)":
                            InsertItemChargeQtyValueCrMemoLine(SalesHeader, SalesCorrLineBefore, SalesCorrLineAfter, TempSalesLine);
                    end;
                UpdateSalesLineFromJobPlanLine(TempSalesLine, JobContractEntryNo);
                TempSalesLine."Job Contract Entry No." := JobContractEntryNo;
                if TempSalesLine.Type <> TempSalesLine.Type::Item then begin
                    TempSalesLine."Purchasing Code" := '';
                    TempSalesLine."Drop Shipment" := false;
                end;
                if TempSalesLine.Modify() then;
                SalesCorrLineBefore."Inconsistent Line" := false;
                SalesCorrLineBefore.Modify();
                SalesCorrLineAfter."Inconsistent Line" := false;
                SalesCorrLineAfter.Modify();

            until (SalesCorrLineBefore.Next() = 0);
    end;

    local procedure InsertSalesLines(var TempSalesLine: Record "Sales Line" temporary)
    var
        SalesLine: Record "Sales Line";
        LinesCounter: Integer;
    begin
        TempSalesLine.Reset();
        if TempSalesLine.FindSet() then
            repeat
                SalesLine.Init();
                SalesLine.TransferFields(TempSalesLine);
                SalesLine.Insert();
                LinesCounter := LinesCounter + 1;
                Window.Update(4, LinesCounter);
                LinesCreated := LinesCounter;
            until TempSalesLine.Next() = 0;
    end;

    local procedure CreateItemChargeAssignmentLines(SalesHeader: Record "Sales Header"; var TempSalesLine: Record "Sales Line" temporary)
    var
        SalesLine: Record "Sales Line";
        SalesInvoiceLine: Record "Sales Invoice Line";
    begin
        TempSalesLine.Reset();
        TempSalesLine.SetRange(Type, TempSalesLine.Type::"Charge (Item)");
        if TempSalesLine.FindSet() then
            repeat
                SalesLine.Get(TempSalesLine."Document Type", TempSalesLine."Document No.", TempSalesLine."Line No.");
                SalesInvoiceLine.Get(TempSalesLine."ITI Corr. Invoice No.", TempSalesLine."ITI Corr. Invoice Line No.");
                CreateCorrInvItemChargeAssignLine(SalesHeader, SalesLine, SalesInvoiceLine);
            until TempSalesLine.Next() = 0;
    end;

    local procedure CreateItemTrackingLines(var TempSalesLine: Record "Sales Line" temporary; ExactCostReverseMandatory: Boolean)
    var
        SalesLine: Record "Sales Line";
        LinesCounter: Integer;
    begin
        LinesCounter := 0;
        TempSalesLine.Reset();
        TempSalesLine.SetRange(Type, TempSalesLine.Type::Item);
        if TempSalesLine.FindSet() then
            repeat
                SalesLine.Get(TempSalesLine."Document Type", TempSalesLine."Document No.", TempSalesLine."Line No.");
                if SalesLine."Appl.-to Item Entry" <> 0 then
                    CreateItemTrackingLine(SalesLine, SalesLine."Appl.-to Item Entry", ExactCostReverseMandatory);
                if SalesLine."Appl.-from Item Entry" <> 0 then
                    CreateItemTrackingLine(SalesLine, SalesLine."Appl.-from Item Entry", ExactCostReverseMandatory);
                LinesCounter := LinesCounter + 1;
                Window.Update(5, LinesCounter);
            until TempSalesLine.Next() = 0;
    end;

    local procedure UpdateSalesDate(var TempSalesLine: Record "Sales Line" temporary; var SalesHeader: Record "Sales Header")
    var
        SalesInvoiceHeader: Record "Sales Invoice Header";
        SalesDate: Date;
    begin
        TempSalesLine.Reset();
        if TempSalesLine.FindFirst() then begin
            TempSalesLine.SetFilter("ITI Corr. Invoice No.", '<>%1', TempSalesLine."ITI Corr. Invoice No.");
            if TempSalesLine.IsEmpty and SalesInvoiceHeader.Get(TempSalesLine."ITI Corr. Invoice No.") then
                SalesDate := SalesInvoiceHeader."ITI Sales Date";
        end;
        if SalesDate <> 0D then begin
            SalesHeader.Validate("ITI Sales Date", SalesDate);
            SalesHeader.Modify();
        end;
    end;

    local procedure CheckCorrLineAgainstCorrReason(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; var TempErrorMessage: Record "Error Message" temporary)
    var
        SalesCorrLineAfter: Record "ITV Sales Corr. Line";
    begin
        GetSalesCorrReason(SalesHeader."ITI Correction Reason");
        SalesCorrLineAfter.Get(
            SalesCorrLineBefore."Document Type",
            SalesCorrLineBefore."Document No.",
            SalesCorrLineBefore."Correction Line No.",
            SalesCorrLineBefore."Before/ After correction"::After);

        if SalesCorrLineBefore.Type <> SalesCorrLineAfter.Type then
            TempErrorMessage.LogSimpleMessage(
                TempErrorMessage."Message Type"::Error,
                StrSubstNo(SameValueBeforeAndAfterErr, SalesCorrLineBefore.FieldCaption(Type), SalesCorrLineBefore."Line No."));

        if SalesCorrLineBefore."No." <> SalesCorrLineAfter."No." then
            TempErrorMessage.LogSimpleMessage(
                TempErrorMessage."Message Type"::Error,
                StrSubstNo(SameValueBeforeAndAfterErr, SalesCorrLineAfter.FieldCaption("No."), SalesCorrLineAfter."Line No."));

        if not (SalesCorrLineAfter.Type in [SalesCorrLineAfter.Type::"G/L Account", SalesCorrLineAfter.Type::Resource]) then
            if Abs(SalesCorrLineBefore.Quantity - SalesCorrLineAfter.Quantity) > Abs(SalesCorrLineBefore.Quantity) then
                TempErrorMessage.LogSimpleMessage(
                    TempErrorMessage."Message Type"::Error,
                    StrSubstNo(AfterCannotBeGreaterErr, SalesCorrLineBefore.FieldCaption(Quantity), SalesCorrLineBefore.Quantity, SalesCorrLineAfter."Line No."));

        case SalesCorrectionReason."Correction Type" of
            SalesCorrectionReason."Correction Type"::Quantity:
                CheckCorrLineForQuantityType(SalesCorrLineBefore, SalesCorrLineAfter, SalesCorrectionReason, TempErrorMessage);
            SalesCorrectionReason."Correction Type"::"Unit Price":
                CheckCorrLineForUnitPriceType(SalesCorrLineBefore, SalesCorrLineAfter, SalesCorrectionReason, TempErrorMessage);
            SalesCorrectionReason."Correction Type"::Discount:
                CheckCorrLineForDiscountType(SalesCorrLineBefore, SalesCorrLineAfter, SalesCorrectionReason, TempErrorMessage);
            SalesCorrectionReason."Correction Type"::"VAT Rate":
                CheckCorrLineForVATRateType(SalesCorrLineBefore, SalesCorrLineAfter, SalesCorrectionReason, TempErrorMessage);
            SalesCorrectionReason."Correction Type"::"Quantity and Value":
                CheckCorrLineForQtyAndValueType(SalesCorrLineBefore, SalesCorrLineAfter, SalesCorrectionReason, TempErrorMessage);
            SalesCorrectionReason."Correction Type"::Cancel:
                CheckCorrLineForCancelType(SalesCorrLineBefore, SalesCorrLineAfter, TempErrorMessage);
        end;
    end;

    local procedure CheckCorrLineForQuantityType(SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; SalesCorrReason: Record "ITI Sales Correction Reason"; var TempErrorMessage: Record "Error Message" temporary)
    begin
        if ((SalesCorrLineAfter."Line Amount" <> SalesCorrLineBefore."Line Amount") and
           (SalesCorrLineAfter.Quantity = SalesCorrLineBefore.Quantity)) or
           (SalesCorrLineAfter."VAT %" <> SalesCorrLineBefore."VAT %") or
           (SalesCorrLineAfter."VAT Prod. Posting Group" <> SalesCorrLineBefore."VAT Prod. Posting Group") or
           (SalesCorrLineAfter."Unit Price" <> SalesCorrLineBefore."Unit Price") or
           (SalesCorrLineAfter."Line Discount %" <> SalesCorrLineBefore."Line Discount %")
        then
            TempErrorMessage.LogSimpleMessage(
                TempErrorMessage."Message Type"::Error,
                StrSubstNo(YouCanChangeOnlyErr, SalesCorrReason."Correction Type", SalesCorrReason.TableCaption, SalesCorrReason.Code));
    end;

    local procedure CheckCorrLineForUnitPriceType(SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; SalesCorrReason: Record "ITI Sales Correction Reason"; var TempErrorMessage: Record "Error Message" temporary)
    begin
        if ((SalesCorrLineAfter."Line Amount" <> SalesCorrLineBefore."Line Amount") and
            (SalesCorrLineAfter."Unit Price" = SalesCorrLineBefore."Unit Price")) or
            (SalesCorrLineAfter."Quantity (Base)" <> SalesCorrLineBefore."Quantity (Base)") or
            (SalesCorrLineAfter."VAT %" <> SalesCorrLineBefore."VAT %") or
            (SalesCorrLineAfter."VAT Prod. Posting Group" <> SalesCorrLineBefore."VAT Prod. Posting Group") or
            (SalesCorrLineAfter."Line Discount %" <> SalesCorrLineBefore."Line Discount %")
        then
            TempErrorMessage.LogSimpleMessage(
                TempErrorMessage."Message Type"::Error,
                StrSubstNo(YouCanChangeOnlyErr, SalesCorrReason."Correction Type", SalesCorrReason.TableCaption, SalesCorrReason.Code));
    end;

    local procedure CheckCorrLineForDiscountType(SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; SalesCorrReason: Record "ITI Sales Correction Reason"; var TempErrorMessage: Record "Error Message" temporary)
    begin
        if ((SalesCorrLineAfter."Line Amount" <> SalesCorrLineBefore."Line Amount") and
            (SalesCorrLineAfter."Line Discount %" = SalesCorrLineBefore."Line Discount %")) or
            (SalesCorrLineAfter."Quantity (Base)" <> SalesCorrLineBefore."Quantity (Base)") or
            (SalesCorrLineAfter."VAT %" <> SalesCorrLineBefore."VAT %") or
            (SalesCorrLineAfter."VAT Prod. Posting Group" <> SalesCorrLineBefore."VAT Prod. Posting Group") or
            (SalesCorrLineAfter."Unit Price" <> SalesCorrLineBefore."Unit Price")
        then
            TempErrorMessage.LogSimpleMessage(
                TempErrorMessage."Message Type"::Error,
                StrSubstNo(YouCanChangeOnlyErr, SalesCorrReason."Correction Type", SalesCorrReason.TableCaption, SalesCorrReason.Code));
    end;

    local procedure CheckCorrLineForVATRateType(SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; SalesCorrReason: Record "ITI Sales Correction Reason"; var TempErrorMessage: Record "Error Message" temporary)
    begin
        if SalesCorrLineAfter."Quantity (Base)" <> SalesCorrLineBefore."Quantity (Base)" then
            TempErrorMessage.LogSimpleMessage(
                TempErrorMessage."Message Type"::Error,
                StrSubstNo(YouCanChangeOnlyErr, SalesCorrReason."Correction Type", SalesCorrReason.TableCaption, SalesCorrReason.Code));
        if SalesCorrLineAfter.Quantity <> SalesCorrLineBefore.Quantity then
            TempErrorMessage.LogSimpleMessage(
                TempErrorMessage."Message Type"::Error,
                 StrSubstNo(YouCanChangeOnlyErr, SalesCorrReason."Correction Type", SalesCorrReason.TableCaption, SalesCorrReason.Code));

    end;

    local procedure CheckCorrLineForQtyAndValueType(SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; SalesCorrReason: Record "ITI Sales Correction Reason"; var TempErrorMessage: Record "Error Message" temporary)
    begin
        if (SalesCorrLineAfter."VAT %" <> SalesCorrLineBefore."VAT %") or
           (SalesCorrLineAfter."VAT Prod. Posting Group" <> SalesCorrLineBefore."VAT Prod. Posting Group")
        then
            TempErrorMessage.LogSimpleMessage(
                TempErrorMessage."Message Type"::Error,
                StrSubstNo(CannotChangeVATPctErr, SalesCorrReason.TableCaption, SalesCorrReason."Correction Type"));
    end;

    local procedure CheckCorrLineForCancelType(SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; var TempErrorMessage: Record "Error Message" temporary)
    var
        PostedSalesCorrLine: Record "ITV Posted Sales Corr. Line";
    begin
        PostedSalesCorrLine.Reset();
        PostedSalesCorrLine.SetCurrentKey("Corr. Invoice No.", "Corr. Invoice Line No.", "Consecutive No.");
        PostedSalesCorrLine.SetRange("Corr. Invoice No.", SalesCorrLineAfter."Document No.");
        PostedSalesCorrLine.SetRange("Corr. Invoice Line No.", SalesCorrLineAfter."Line No.");
        if not PostedSalesCorrLine.IsEmpty then
            TempErrorMessage.LogSimpleMessage(
                TempErrorMessage."Message Type"::Error,
                StrSubstNo(CannotCreateCorrWhenCancelErr, SalesCorrLineAfter."Document No."));

        if (SalesCorrLineAfter."Quantity (Base)" <> 0) or
           (SalesCorrLineAfter.Amount <> 0) or
           (SalesCorrLineAfter."VAT Base Amount" <> 0) or
           (SalesCorrLineAfter."Unit Price" <> 0) or
           (SalesCorrLineAfter.Description <> SalesCorrLineBefore.Description) or
           (SalesCorrLineAfter."VAT Prod. Posting Group" <> SalesCorrLineBefore."VAT Prod. Posting Group") or
           (SalesCorrLineAfter."VAT %" <> SalesCorrLineBefore."VAT %")

        then
            TempErrorMessage.LogSimpleMessage(
                TempErrorMessage."Message Type"::Error, CannotChangeAfterWhenCancelErr);
    end;

    local procedure StopIfErrors(var TempErrorMessage: Record "Error Message" temporary)
    begin
        if TempErrorMessage.HasErrors(true) then begin
            Window.Close();
            TempErrorMessage.ShowErrorMessages(true);
        end;
    end;

    local procedure Finish(var TempSalesLine: Record "Sales Line" temporary; TotalLines: Integer)
    begin
        TempSalesLine.Reset();
        TempSalesLine.DeleteAll();
        Window.Close();
        Message(SalesCrMemoLinesCreatedMsg, LinesCreated, TotalLines);
    end;

    local procedure InsertVATRateCrMemoLines(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; var TempSalesLine: Record "Sales Line" temporary)
    var
        SalesLine: Record "Sales Line";
    begin
        InitSalesLine(SalesHeader, SalesCorrLineAfter, SalesLine);
        SalesLine.Type := SalesLine.Type::"G/L Account";
        SalesLine."No." := GetSalesVATAccount(SalesLine."VAT Bus. Posting Group", SalesLine."VAT Prod. Posting Group");
        SalesLine.Validate("Unit of Measure Code", '');
        SalesLine.Validate(Quantity, SalesCorrLineBefore.Quantity);
        SalesLine.Validate("Unit Price", SalesCorrLineBefore."Unit Price");
        SalesLine.Validate("Line Discount %", SalesCorrLineBefore."Line Discount %");
        if (SalesLine.Quantity <> 0) and (SalesLine."Unit Price" <> 0) then
            SalesLine.Validate("Line Amount", SalesCorrLineBefore."Line Amount");
        SalesLine.UpdateAmounts();
        SalesLine.Validate("Unit Cost (LCY)", 0);
        SalesLine."VAT Base Amount" := SalesCorrLineBefore."VAT Base Amount";
        SalesLine.Validate("VAT Prod. Posting Group", SalesCorrLineBefore."VAT Prod. Posting Group");
        SalesLine.InitOutstandingAmount();
        SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
        UpdateSalesLine(SalesHeader, SalesLine);
        SalesLine."Blanket Order No." := '';
        SalesLine."Blanket Order Line No." := 0;
        if SalesLine."Line Amount" <> 0 then
            InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);

        InitSalesLine(SalesHeader, SalesCorrLineAfter, SalesLine);
        SalesLine.Type := SalesLine.Type::"G/L Account";
        SalesLine."No." := GetSalesVATAccount(SalesLine."VAT Bus. Posting Group", SalesLine."VAT Prod. Posting Group");
        SalesLine.Validate("Unit of Measure Code", '');
        SalesLine.Validate(Quantity, -SalesCorrLineAfter.Quantity);
        SalesLine.Validate("Unit Price", SalesCorrLineAfter."Unit Price");
        SalesLine.Validate("Line Discount %", SalesCorrLineAfter."Line Discount %");

        if (SalesLine.Quantity <> 0) and (SalesLine."Unit Price" <> 0) then
            SalesLine.Validate("Line Amount", -SalesCorrLineAfter."Line Amount");
        SalesLine.UpdateAmounts();
        SalesLine.Validate("Unit Cost (LCY)", 0);
        SalesLine."VAT Base Amount" := -SalesCorrLineAfter."VAT Base Amount";
        SalesLine.Validate("VAT Prod. Posting Group", SalesCorrLineAfter."VAT Prod. Posting Group");
        SalesLine.InitOutstandingAmount();
        SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
        UpdateSalesLine(SalesHeader, SalesLine);
        SalesLine."Blanket Order No." := '';
        SalesLine."Blanket Order Line No." := 0;
        if SalesLine."Line Amount" <> 0 then
            InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);
    end;

    local procedure InsertGLAccQtyOrValueCrMemoLine(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; var TempSalesLine: Record "Sales Line" temporary)
    var
        SalesLine: Record "Sales Line";
    begin
        SalesCorrLineAfter.TestField(Type, SalesCorrLineAfter.Type::"G/L Account");
        InitSalesLine(SalesHeader, SalesCorrLineBefore, SalesLine);
        if (SalesCorrLineBefore.Quantity - SalesCorrLineAfter.Quantity) <> 0 then begin
            SalesLine.Validate(Quantity, SalesCorrLineBefore.Quantity - SalesCorrLineAfter.Quantity);
            SalesLine.Validate("Unit Price", (SalesCorrLineBefore."Line Amount" - SalesCorrLineAfter."Line Amount") / SalesLine.Quantity);

        end else begin
            SalesLine.Validate(Quantity, 1);
            SalesLine.Validate("Unit Price", SalesCorrLineBefore."Line Amount" - SalesCorrLineAfter."Line Amount");
        end;
        SalesLine.Validate("Line Discount %", 0);
        if (SalesLine.Quantity <> 0) and (SalesLine."Unit Price" <> 0) then
            SalesLine.Validate("Line Amount", SalesCorrLineBefore."Line Amount" - SalesCorrLineAfter."Line Amount");
        SalesLine.UpdateAmounts();
        SalesLine.Validate("Unit Cost (LCY)", 0);
        SalesLine."VAT Base Amount" := 0;
        SalesLine.InitOutstandingAmount();
        SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount";
        UpdateSalesLine(SalesHeader, SalesLine);
        if SalesLine."Line Amount" <> 0 then
            InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);


    end;

    local procedure InsertItemQtyValueCrMemoLines(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; SalesCorrReason: Record "ITI Sales Correction Reason"; var TempSalesLine: Record "Sales Line" temporary; ExactCostReverseMandatory: Boolean)
    var
        SalesInvoiceLine: Record "Sales Invoice Line";
        TempItemLedgerEntry: Record "Item Ledger Entry" temporary;
    begin
        SalesCorrLineAfter.TestField(Type, SalesCorrLineAfter.Type::Item);

        case true of
            SalesCorrLineBefore.Quantity = SalesCorrLineAfter.Quantity:
                InsertItemValueChangeCrMemoLine(SalesHeader, SalesCorrLineBefore, SalesCorrLineAfter, SalesCorrReason, TempSalesLine);
            SalesCorrLineBefore.Quantity <> SalesCorrLineAfter.Quantity:
                if SalesCorrLineBefore."Unit Price" <> SalesCorrLineAfter."Unit Price" then begin
                    SalesInvoiceLine.Get(SalesCorrLineBefore."Corr. Invoice No.", SalesCorrLineBefore."Corr. Invoice Line No.");
                    SalesInvoiceLine.GetItemLedgEntries(TempItemLedgerEntry, true);
                    if TempItemLedgerEntry.FindSet() then
                        InsertItemPriceQtyCrMemoLine(SalesHeader, SalesCorrLineBefore, SalesCorrLineAfter, TempSalesLine, ExactCostReverseMandatory, SalesCorrReason)
                end else begin
                    SalesInvoiceLine.Get(SalesCorrLineBefore."Corr. Invoice No.", SalesCorrLineBefore."Corr. Invoice Line No.");
                    SalesInvoiceLine.GetItemLedgEntries(TempItemLedgerEntry, true);
                    if TempItemLedgerEntry.FindSet() then
                        InsertItemLedgerEntryApplCrMemoLines(SalesHeader, SalesCorrLineBefore, SalesCorrLineAfter, TempItemLedgerEntry, TempSalesLine, ExactCostReverseMandatory)
                    else
                        InsertNotApplItemCrMemoLines(SalesHeader, SalesCorrLineBefore, SalesCorrLineAfter, TempSalesLine);
                end;
        end;
    end;

    local procedure InsertItemValueChangeCrMemoLine(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; SalesCorrReason: Record "ITI Sales Correction Reason"; var TempSalesLine: Record "Sales Line" temporary)
    var
        SalesLine: Record "Sales Line";
        UseItemCharge: Boolean;

    begin
        // Item value only change
        if SalesCorrLineBefore.Quantity = 0 then
            exit;
        if (SalesCorrLineBefore."Corr. Invoice No." <> '') and
            (SalesCorrLineBefore."Corr. Invoice Line No." <> 0)
        then
            UseItemCharge := true    // Sales Invoice Line exists - use Charge (Item);
        else
            UseItemCharge := false;   // Sales Invoice Line does not exists - use G/L Account;
        InitSalesLine(SalesHeader, SalesCorrLineAfter, SalesLine);
        if UseItemCharge then begin
            SalesLine.Type := SalesLine.Type::"Charge (Item)";
            SalesLine."Allow Item Charge Assignment" := false;
            SalesLine."No." := SalesCorrReason."Item Charge Code";
        end else begin
            SalesLine.Type := SalesLine.Type::"G/L Account";
            SalesLine."No." := GetSalesAccount(SalesLine);
        end;
        SalesLine.Validate("Unit of Measure Code", '');
        SalesLine.Validate(Quantity, 1);
        SalesLine.Validate("Unit Price", SalesCorrLineBefore."Line Amount" - SalesCorrLineAfter."Line Amount");
        SalesLine.Validate("Line Discount %", 0);
        IF (SalesLine.Quantity <> 0) AND (SalesLine."Unit Price" <> 0) THEN
            SalesLine.VALIDATE("Line Amount", SalesCorrLineBefore."Line Amount" - SalesCorrLineAfter."Line Amount");
        SalesLine.UpdateAmounts();
        SalesLine.Validate("Unit Cost (LCY)", 0);
        SalesLine."VAT Base Amount" := 0;
        SalesLine.InitOutstandingAmount();
        SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
        UpdateSalesLine(SalesHeader, SalesLine);
        SalesLine."Blanket Order No." := '';
        SalesLine."Blanket Order Line No." := 0;
        if SalesLine."Line Amount" <> 0 then
            InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);
        /**
                //poczatek drugiej linii
                InitSalesLine(SalesHeader, SalesCorrLineAfter, SalesLine);
                if UseItemCharge then begin
                    SalesLine.Type := SalesLine.Type::"Charge (Item)";
                    SalesLine."Allow Item Charge Assignment" := false;
                    SalesLine."No." := SalesCorrReason."Item Charge Code";
                end else begin
                    SalesLine.Type := SalesLine.Type::"G/L Account";
                    SalesLine."No." := GetSalesAccount(SalesLine);
                end;
                SalesLine.Validate("Unit of Measure Code", '');
                SalesLine.Validate(Quantity, -SalesCorrLineAfter.Quantity);
                SalesLine.Validate("Unit Price", SalesCorrLineAfter."Unit Price");
                SalesLine.Validate("Line Discount %", SalesCorrLineAfter."Line Discount %");
                SalesLine.UpdateAmounts();
                SalesLine.Validate("Unit Cost (LCY)", 0);
                SalesLine."VAT Base Amount" := 0;
                SalesLine.InitOutstandingAmount();
                SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
                UpdateSalesLine(SalesHeader, SalesLine);
                SalesLine."Blanket Order No." := '';
                SalesLine."Blanket Order Line No." := 0;
                if SalesLine."Line Amount" <> 0 then
                    InsertTempSalesLine(SalesLine, SalesCorrLineAfter, TempSalesLine);
                    */
    end;

    local procedure InsertItemPriceQtyCrMemoLine(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; var TempSalesLine: Record "Sales Line" temporary; ExactCostReverseMandatory: Boolean; SalesCorrReason: Record "ITI Sales Correction Reason")
    var
        SalesLine: Record "Sales Line";
        TempItemLedgerEntry: Record "Item Ledger Entry" temporary;
        SalesInvoiceLine: Record "Sales Invoice Line";
        NextLineNo: Integer;
        QtyToReturnBase: Decimal;
        ShippedQuantityNotReturned: Decimal;
        OldPrice: Decimal;
        NewPrice: Decimal;
        NewQuantity: Integer;
        UseItemCharge: Boolean;

    begin
        OldPrice := SalesCorrLineBefore."Unit Price";
        NewPrice := SalesCorrLineAfter."Unit Price";
        NewQuantity := SalesCorrLineAfter.Quantity;
        SalesCorrLineAfter.TestField(Type, SalesCorrLineAfter.Type::Item);
        //Value and quantity change
        SalesInvoiceLine.Get(SalesCorrLineBefore."Corr. Invoice No.", SalesCorrLineBefore."Corr. Invoice Line No.");
        SalesInvoiceLine.GetItemLedgEntries(TempItemLedgerEntry, true);
        if TempItemLedgerEntry.FindSet() then begin
            FindNextLineNo(TempSalesLine, NextLineNo);
            QtyToReturnBase := SalesCorrLineBefore."Quantity (Base)" - SalesCorrLineAfter."Quantity (Base)";

            repeat
                InitSalesLine(SalesHeader, SalesCorrLineBefore, SalesLine);
                ShippedQuantityNotReturned := TempItemLedgerEntry.Quantity;
                if Abs(QtyToReturnBase) >= Abs(ShippedQuantityNotReturned) then
                    SalesLine.Validate(Quantity,
                      Round((-ShippedQuantityNotReturned / SalesCorrLineAfter."Qty. per Unit of Measure"), 0.00001))
                else
                    SalesLine.Validate(Quantity,
                      Round((QtyToReturnBase / SalesCorrLineAfter."Qty. per Unit of Measure"), 0.00001));
                if TempItemLedgerEntry.Quantity > 0 then
                    QtyToReturnBase := QtyToReturnBase + SalesLine."Quantity (Base)"
                else
                    QtyToReturnBase := QtyToReturnBase - SalesLine."Quantity (Base)";
                SalesLine.Validate("Line Discount %", SalesCorrLineBefore."Line Discount %");
                if (SalesLine.Quantity <> 0) then
                    SalesLine.Validate("Unit Price", OldPrice);
                SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
                if ExactCostReverseMandatory then
                    if TempItemLedgerEntry.Quantity > 0 then
                        SalesLine."Appl.-to Item Entry" := TempItemLedgerEntry."Entry No."
                    else
                        SalesLine."Appl.-from Item Entry" := TempItemLedgerEntry."Entry No.";
                SalesLine."VAT Base Amount" := 0;
                UpdateSalesLine(SalesHeader, SalesLine);
                if SalesLine.Quantity <> 0 then begin
                    CheckNegativeQty(SalesLine);
                    NextLineNo := NextLineNo + 10000;
                    SalesLine."Line No." := NextLineNo;
                    InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);
                end;
            until TempItemLedgerEntry.Next() = 0;
        end else begin
            // No linked Value Entry exists
            InitSalesLine(SalesHeader, SalesCorrLineBefore, SalesLine);
            SalesLine.Validate(Quantity, SalesCorrLineBefore."Quantity (Base)" - SalesCorrLineAfter."Quantity (Base)");
            SalesLine.Validate("Line Discount %", SalesCorrLineBefore."Line Discount %");
            if (SalesLine.Quantity <> 0) then
                SalesLine.Validate("Unit Price", OldPrice)
            else
                SalesLine.Validate("Unit Price", 0);
            SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
            UpdateSalesLine(SalesHeader, SalesLine);
            if SalesLine.Quantity <> 0 then begin
                CheckNegativeQty(SalesLine);
                InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);
            end;
        end;

        if SalesCorrLineBefore.Quantity = 0 then
            exit;
        if (SalesCorrLineBefore."Corr. Invoice No." <> '') and
            (SalesCorrLineBefore."Corr. Invoice Line No." <> 0)
        then
            UseItemCharge := true    // Sales Invoice Line exists - use Charge (Item);
        else
            UseItemCharge := false;   // Sales Invoice Line does not exists - use G/L Account;
        InitSalesLine(SalesHeader, SalesCorrLineAfter, SalesLine);
        if UseItemCharge then begin
            SalesLine.Type := SalesLine.Type::"Charge (Item)";
            SalesLine."No." := SalesCorrReason."Item Charge Code";
        end else begin
            SalesLine.Type := SalesLine.Type::"G/L Account";
            SalesLine."No." := GetSalesAccount(SalesLine);
        end;
        SalesLine.Validate("Unit of Measure Code", '');
        SalesLine.Validate(Quantity, NewQuantity);
        SalesLine.Validate("Unit Price", OldPrice);
        SalesLine.Validate("Line Discount %", SalesCorrLineBefore."Line Discount %");
        SalesLine.UpdateAmounts();
        SalesLine.Validate("Unit Cost (LCY)", 0);
        SalesLine."VAT Base Amount" := 0;
        SalesLine.InitOutstandingAmount();
        SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
        UpdateSalesLine(SalesHeader, SalesLine);
        SalesLine."Blanket Order No." := '';
        SalesLine."Blanket Order Line No." := 0;
        if SalesLine."Line Amount" <> 0 then
            InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);

        InitSalesLine(SalesHeader, SalesCorrLineAfter, SalesLine);
        if UseItemCharge then begin
            SalesLine.Type := SalesLine.Type::"Charge (Item)";
            SalesLine."No." := SalesCorrReason."Item Charge Code";
        end else begin
            SalesLine.Type := SalesLine.Type::"G/L Account";
            SalesLine."No." := GetSalesAccount(SalesLine);
        end;
        SalesLine.Validate("Unit of Measure Code", '');
        SalesLine.Validate(Quantity, -NewQuantity);
        SalesLine.Validate("Unit Price", NewPrice);
        SalesLine.Validate("Line Discount %", SalesCorrLineAfter."Line Discount %");
        SalesLine.UpdateAmounts();
        SalesLine.Validate("Unit Cost (LCY)", 0);
        SalesLine."VAT Base Amount" := 0;
        SalesLine.InitOutstandingAmount();
        SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
        UpdateSalesLine(SalesHeader, SalesLine);
        SalesLine."Blanket Order No." := '';
        SalesLine."Blanket Order Line No." := 0;
        if SalesLine."Line Amount" <> 0 then
            InsertTempSalesLine(SalesLine, SalesCorrLineAfter, TempSalesLine);

    end;

    local procedure InsertItemLedgerEntryApplCrMemoLines(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; var TempItemLedgerEntry: Record "Item Ledger Entry" temporary; var TempSalesLine: Record "Sales Line" temporary; ExactCostReverseMandatory: Boolean)
    var
        SalesLine: Record "Sales Line";
        QtyToReturn: Decimal;
        QtyToReturnBase: Decimal;
        ShippedQuantityNotReturned: Decimal;
        NextLineNo: Integer;
    begin
        FindNextLineNo(TempSalesLine, NextLineNo);
        QtyToReturn := SalesCorrLineBefore.Quantity - SalesCorrLineAfter.Quantity;
        QtyToReturnBase := SalesCorrLineBefore."Quantity (Base)" - SalesCorrLineAfter."Quantity (Base)";
        repeat
            InitSalesLine(SalesHeader, SalesCorrLineBefore, SalesLine);
            ShippedQuantityNotReturned := TempItemLedgerEntry.Quantity + CalculateShippedQuantityNotReturned(TempItemLedgerEntry);
            if Abs(QtyToReturnBase) >= Abs(ShippedQuantityNotReturned) then
                SalesLine.Validate(Quantity,
                  Round((-ShippedQuantityNotReturned / SalesCorrLineAfter."Qty. per Unit of Measure"), 0.00001))
            else
                SalesLine.Validate(Quantity, Round((QtyToReturnBase / SalesCorrLineAfter."Qty. per Unit of Measure"), 0.00001));
            if TempItemLedgerEntry.Quantity > 0 then
                QtyToReturnBase := QtyToReturnBase + SalesLine."Quantity (Base)"
            else
                QtyToReturnBase := QtyToReturnBase - SalesLine."Quantity (Base)";
            SalesLine.Validate("Line Discount %", SalesCorrLineAfter."Line Discount %");
            if (SalesLine.Quantity <> 0) then
                SalesLine.Validate("Unit Price", ((SalesCorrLineBefore."Line Amount" - SalesCorrLineAfter."Line Amount") / QtyToReturn) * 100 / (100 - SalesLine."Line Discount %"));
            SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
            if ExactCostReverseMandatory then
                if TempItemLedgerEntry.Quantity > 0 then
                    SalesLine."Appl.-to Item Entry" := TempItemLedgerEntry."Entry No."
                else
                    SalesLine."Appl.-from Item Entry" := TempItemLedgerEntry."Entry No.";
            SalesLine."VAT Base Amount" := 0;
            UpdateSalesLine(SalesHeader, SalesLine);
            if SalesLine.Quantity <> 0 then begin
                NextLineNo := NextLineNo + 10000;
                SalesLine."Line No." := NextLineNo;
                InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);
            end;
        until TempItemLedgerEntry.Next() = 0;
    end;

    local procedure InsertNotApplItemCrMemoLines(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; var TempSalesLine: Record "Sales Line" temporary)
    var
        SalesLine: Record "Sales Line";
    begin
        InitSalesLine(SalesHeader, SalesCorrLineBefore, SalesLine);
        SalesLine.Validate(Quantity, SalesCorrLineBefore."Quantity (Base)" - SalesCorrLineAfter."Quantity (Base)");
        SalesLine.Validate("Line Discount %", 0);
        if (SalesLine.Quantity <> 0) then
            SalesLine.Validate("Unit Price", (SalesCorrLineBefore."Line Amount" - SalesCorrLineAfter."Line Amount") / SalesLine.Quantity)
        else
            SalesLine.Validate("Unit Price", 0);
        SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
        UpdateSalesLine(SalesHeader, SalesLine);
        if SalesLine.Quantity <> 0 then begin
            CheckNegativeQty(SalesLine);
            InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);
        end;
    end;

    local procedure InsertItemQtyCrMemoLine(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; var TempSalesLine: Record "Sales Line" temporary; ExactCostReverseMandatory: Boolean)
    var
        SalesLine: Record "Sales Line";
        TempItemLedgerEntry: Record "Item Ledger Entry" temporary;
        SalesInvoiceLine: Record "Sales Invoice Line";
        NextLineNo: Integer;
        QtyToReturnBase: Decimal;
        ShippedQuantityNotReturned: Decimal;
    begin
        SalesCorrLineAfter.TestField(Type, SalesCorrLineAfter.Type::Item);
        SalesInvoiceLine.Get(SalesCorrLineBefore."Corr. Invoice No.", SalesCorrLineBefore."Corr. Invoice Line No.");
        SalesInvoiceLine.GetItemLedgEntries(TempItemLedgerEntry, true);
        if TempItemLedgerEntry.FindSet() then begin
            FindNextLineNo(TempSalesLine, NextLineNo);
            QtyToReturnBase := SalesCorrLineBefore."Quantity (Base)" - SalesCorrLineAfter."Quantity (Base)";

            repeat
                InitSalesLine(SalesHeader, SalesCorrLineBefore, SalesLine);
                ShippedQuantityNotReturned := TempItemLedgerEntry.Quantity;
                if Abs(QtyToReturnBase) >= Abs(ShippedQuantityNotReturned) then
                    SalesLine.Validate(Quantity,
                      Round((-ShippedQuantityNotReturned / SalesCorrLineAfter."Qty. per Unit of Measure"), 0.00001))
                else
                    SalesLine.Validate(Quantity,
                      Round((QtyToReturnBase / SalesCorrLineAfter."Qty. per Unit of Measure"), 0.00001));
                if TempItemLedgerEntry.Quantity > 0 then
                    QtyToReturnBase := QtyToReturnBase + SalesLine."Quantity (Base)"
                else
                    QtyToReturnBase := QtyToReturnBase - SalesLine."Quantity (Base)";
                SalesLine.Validate("Line Discount %", SalesCorrLineBefore."Line Discount %");
                if (SalesLine.Quantity <> 0) then
                    SalesLine.Validate("Unit Price", SalesCorrLineBefore."Unit Price");
                SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
                if ExactCostReverseMandatory then
                    if TempItemLedgerEntry.Quantity > 0 then
                        SalesLine."Appl.-to Item Entry" := TempItemLedgerEntry."Entry No."
                    else
                        SalesLine."Appl.-from Item Entry" := TempItemLedgerEntry."Entry No.";
                SalesLine."VAT Base Amount" := 0;
                UpdateSalesLine(SalesHeader, SalesLine);
                if SalesLine.Quantity <> 0 then begin
                    CheckNegativeQty(SalesLine);
                    NextLineNo := NextLineNo + 10000;
                    SalesLine."Line No." := NextLineNo;
                    InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);
                end;
            until TempItemLedgerEntry.Next() = 0;
        end else begin
            InitSalesLine(SalesHeader, SalesCorrLineBefore, SalesLine);
            SalesLine.Validate(Quantity, SalesCorrLineBefore."Quantity (Base)" - SalesCorrLineAfter."Quantity (Base)");
            SalesLine.Validate("Line Discount %", SalesCorrLineBefore."Line Discount %");
            if (SalesLine.Quantity <> 0) then
                SalesLine.Validate("Unit Price", SalesCorrLineBefore."Unit Price")
            else
                SalesLine.Validate("Unit Price", 0);
            SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
            UpdateSalesLine(SalesHeader, SalesLine);
            if SalesLine.Quantity <> 0 then begin
                CheckNegativeQty(SalesLine);
                InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);
            end;
        end;
    end;

    local procedure InsertResourceQtyValueCrMemoLine(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; var TempSalesLine: Record "Sales Line" temporary)
    var
        OldSalesLine: Record "Sales Line";
        NewSalesLine: Record "Sales Line";
    begin
        SalesCorrLineAfter.TestField(Type, SalesCorrLineAfter.Type::Resource);
        InitSalesLine(SalesHeader, SalesCorrLineBefore, OldSalesLine);
        if (SalesCorrLineBefore.Quantity - SalesCorrLineAfter.Quantity) <> 0 then begin
            OldSalesLine.Validate(Quantity, SalesCorrLineBefore.Quantity);
            OldSalesLine.Validate("Unit Price", SalesCorrLineBefore."Unit Price");
        end else begin
            OldSalesLine.Validate(Quantity, SalesCorrLineBefore.Quantity);
            OldSalesLine.Validate("Unit Price", SalesCorrLineBefore."Unit Price");
        end;
        OldSalesLine.Validate("Line Discount %", SalesCorrLineBefore."Line Discount %");
        OldSalesLine.UpdateAmounts();
        OldSalesLine."VAT Base Amount" := 0;
        OldSalesLine.InitOutstandingAmount();
        OldSalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount";
        UpdateSalesLine(SalesHeader, OldSalesLine);
        if OldSalesLine."Line Amount" <> 0 then
            InsertTempSalesLine(OldSalesLine, SalesCorrLineBefore, TempSalesLine);

        InitSalesLine(SalesHeader, SalesCorrLineAfter, NewSalesLine);
        if (SalesCorrLineBefore.Quantity - SalesCorrLineAfter.Quantity) <> 0 then begin
            NewSalesLine.Validate(Quantity, -SalesCorrLineAfter.Quantity);
            NewSalesLine.Validate("Unit Price", SalesCorrLineAfter."Unit Price");
        end else begin
            NewSalesLine.Validate(Quantity, -SalesCorrLineAfter.Quantity);
            NewSalesLine.Validate("Unit Price", SalesCorrLineAfter."Unit Price");
        end;
        NewSalesLine.Validate("Line Discount %", SalesCorrLineAfter."Line Discount %");
        NewSalesLine.UpdateAmounts();
        NewSalesLine."VAT Base Amount" := 0;
        NewSalesLine.InitOutstandingAmount();
        NewSalesLine."Inv. Discount Amount" := SalesCorrLineAfter."Inv. Discount Amount";
        UpdateSalesLine(SalesHeader, NewSalesLine);
        if NewSalesLine."Line Amount" <> 0 then
            InsertTempSalesLine(NewSalesLine, SalesCorrLineAfter, TempSalesLine);
    end;

    local procedure InsertFixedAssetQtyValueCrMemoLine(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; var TempSalesLine: Record "Sales Line" temporary)
    var
        SalesLine: Record "Sales Line";
    begin
        SalesCorrLineAfter.TestField(Type, SalesCorrLineAfter.Type::"Fixed Asset");
        InitSalesLine(SalesHeader, SalesCorrLineAfter, SalesLine);
        SalesLine.Validate(Quantity, 1);
        SalesLine.Validate("Unit Price", SalesCorrLineBefore."Line Amount" - SalesCorrLineAfter."Line Amount");
        SalesLine.Validate("Line Discount %", 0);
        if (SalesLine.Quantity <> 0) and (SalesLine."Unit Price" <> 0) then
            SalesLine.Validate("Line Amount", SalesCorrLineBefore."Line Amount" - SalesCorrLineAfter."Line Amount");
        SalesLine.UpdateAmounts();
        SalesLine.Validate("Unit Cost (LCY)", 0);
        SalesLine."VAT Base Amount" := 0;
        SalesLine.InitOutstandingAmount();
        SalesLine."Inv. Discount Amount" := SalesCorrLineBefore."Inv. Discount Amount" - SalesCorrLineAfter."Inv. Discount Amount";
        UpdateSalesLine(SalesHeader, SalesLine);
        if SalesLine."Line Amount" <> 0 then
            InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);
    end;

    local procedure InsertItemChargeQtyValueCrMemoLine(SalesHeader: Record "Sales Header"; SalesCorrLineBefore: Record "ITV Sales Corr. Line"; SalesCorrLineAfter: Record "ITV Sales Corr. Line"; var TempSalesLine: Record "Sales Line" temporary)
    var
        SalesLine: Record "Sales Line";
    begin
        SalesCorrLineBefore.TestField(Type, SalesCorrLineBefore.Type::"Charge (Item)");
        InitSalesLine(SalesHeader, SalesCorrLineBefore, SalesLine);
        SalesLine.Type := SalesLine.Type::"Charge (Item)";
        SalesLine."No." := SalesCorrLineBefore."No.";
        SalesLine.Validate(Quantity, SalesCorrLineBefore.Quantity);
        SalesLine.Validate("Unit Price", SalesCorrLineBefore."Unit Price");
        SalesLine.Validate("Line Discount %", SalesCorrLineBefore."Line Discount %");
        if (SalesLine.Quantity <> 0) and (SalesLine."Unit Price" <> 0) then
            SalesLine.Validate("Line Amount", SalesCorrLineBefore."Line Amount");
        SalesLine.UpdateAmounts();
        SalesLine.validate("Unit Cost", 0);
        SalesLine."VAT Base Amount" := 0;
        SalesLine.InitOutstandingAmount();
        UpdateSalesLine(SalesHeader, SalesLine);
        if SalesLine."Line Amount" <> 0 then
            InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);


        SalesCorrLineAfter.TestField(Type, SalesCorrLineAfter.Type::"Charge (Item)");
        InitSalesLine(SalesHeader, SalesCorrLineAfter, SalesLine);
        SalesLine.Type := SalesLine.Type::"Charge (Item)";
        SalesLine."No." := SalesCorrLineAfter."No.";
        SalesLine.Validate(Quantity, -SalesCorrLineAfter.Quantity);
        SalesLine.Validate("Unit Price", SalesCorrLineAfter."Unit Price");
        SalesLine.Validate("Line Discount %", SalesCorrLineAfter."Line Discount %");
        SalesLine.UpdateAmounts();
        SalesLine.Validate("Unit Cost (LCY)", 0);
        SalesLine."VAT Base Amount" := 0;
        SalesLine.InitOutstandingAmount();
        UpdateSalesLine(SalesHeader, SalesLine);
        if SalesLine."Line Amount" <> 0 then
            InsertTempSalesLine(SalesLine, SalesCorrLineBefore, TempSalesLine);
    end;

    local procedure InitSalesLine(SalesHeader: Record "Sales Header"; SalesCorrLine: Record "ITV Sales Corr. Line"; var SalesLine: Record "Sales Line")
    var
        SalesInvoiceLine: Record "Sales Invoice Line";
    begin
        SalesLine.Init();
        SalesInvoiceLine.Get(SalesCorrLine."Corr. Invoice No.", SalesCorrLine."Corr. Invoice Line No.");
        SalesLine.TransferFields(SalesInvoiceLine);
        SalesLine.TransferFields(SalesCorrLine);
        LastLineNo := LastLineNo + 10000;
        SalesLine."Line No." := LastLineNo;
        SalesLine."Sell-to Customer No." := SalesHeader."Sell-to Customer No.";
        SalesLine.Quantity := 0;
        SalesLine."Line Discount %" := 0;
        SalesLine."Line Discount Amount" := 0;
        SalesLine."Shipment No." := '';
        SalesLine."Shipment Line No." := 0;
        SalesLine.Validate("Location Code", SalesInvoiceLine."Location Code");
        SalesLine."Unit Cost (LCY)" := SalesInvoiceLine."Unit Cost (LCY)";
        SalesLine."ITI Corr. Invoice No." := SalesCorrLine."Corr. Invoice No.";
        SalesLine."ITI Corr. Invoice Line No." := SalesCorrLine."Corr. Invoice Line No.";
        SalesLine."ITV Correction Line No." := SalesCorrLine."Correction Line No.";
    end;

    local procedure UpdateSalesLine(SalesHeader: Record "Sales Header"; var SalesLine: Record "Sales Line")
    var
        SalesLine2: Record "Sales Line";
        TempVATAmountLine0: Record "VAT Amount Line" temporary;
        TempVATAmountLine1: Record "VAT Amount Line" temporary;
    begin
        SalesLine2 := SalesLine;
        SalesLine2.SetSalesHeader(SalesHeader);
        SalesLine2.CalcVATAmountLines(0, SalesHeader, SalesLine, TempVATAmountLine0);
        SalesLine2.CalcVATAmountLines(1, SalesHeader, SalesLine, TempVATAmountLine1);
        SalesLine2.UpdateVATOnLines(0, SalesHeader, SalesLine, TempVATAmountLine0);
        SalesLine2.UpdateVATOnLines(1, SalesHeader, SalesLine, TempVATAmountLine1);
        SalesLine := SalesLine2;
        SalesLine.InitOutstanding();
        SalesLine.InitQtyToReceive();
    end;

    local procedure GetSalesAccount(SalesLine: Record "Sales Line"): Code[20]
    var
        GeneralPostingSetup: Record "General Posting Setup";
    begin
        GeneralPostingSetup.Get(SalesLine."VAT Bus. Posting Group", SalesLine."VAT Prod. Posting Group");
        GeneralPostingSetup.TestField("Sales Account");
        exit(GeneralPostingSetup.GetSalesAccount());
    end;


    local procedure GetSalesVATAccount(VATBusPostingGroup: Code[20]; VATProdPostingGroup: Code[20]): Code[20]
    var
        VATPostingSetup: Record "VAT Posting Setup";
    begin
        VATPostingSetup.Get(VATBusPostingGroup, VATProdPostingGroup);
        VATPostingSetup.TestField("Sales VAT Account");
        exit(VATPostingSetup."Sales VAT Account");
    end;

    local procedure InsertTempSalesLine(SalesLine: Record "Sales Line"; SalesCorrLine: Record "ITV Sales Corr. Line"; var TempSalesLine: Record "Sales Line" temporary)
    begin
        TempSalesLine.Init();
        TempSalesLine.TransferFields(SalesLine);
        TempSalesLine."ITI Corr. Invoice No." := SalesCorrLine."Corr. Invoice No.";
        TempSalesLine."ITI Corr. Invoice Line No." := SalesCorrLine."Corr. Invoice Line No.";
        TempSalesLine.Insert();
    end;

    local procedure FindNextLineNo(var TempSalesLine: Record "Sales Line"; var NextLineNo: Integer)
    begin
        if TempSalesLine.FindLast() then
            NextLineNo := TempSalesLine."Line No."
        else
            NextLineNo := 0;
    end;

    local procedure CheckNegativeQty(SalesLine: Record "Sales Line")
    begin
        if SalesLine.Quantity < 0 then
            Error(QtyDiffCannotBeNegativeErr, SalesLine."No.", SalesLine."ITV Correction Line No.");
    end;

    local procedure CalculateShippedQuantityNotReturned(ItemLedgerEntry: Record "Item Ledger Entry") ShippedQuantityNotReturned: Decimal
    var
        ItemApplicationEntry: Record "Item Application Entry";
        ItemApplicationEntry2: Record "Item Application Entry";
    begin
        ItemApplicationEntry.SetRange("Outbound Item Entry No.", ItemLedgerEntry."Entry No.");
        ItemApplicationEntry.SetFilter("Item Ledger Entry No.", '<>%1', ItemLedgerEntry."Entry No.");
        if ItemApplicationEntry.FindSet() then
            repeat
                ShippedQuantityNotReturned += ItemApplicationEntry.Quantity;
                if ItemApplicationEntry."Item Ledger Entry No." = ItemApplicationEntry."Inbound Item Entry No." then begin
                    ItemApplicationEntry2.SetRange("Inbound Item Entry No.", ItemApplicationEntry."Inbound Item Entry No.");
                    ItemApplicationEntry2.SetFilter("Outbound Item Entry No.", '<>%1', ItemLedgerEntry."Entry No.");
                    if ItemApplicationEntry2.FindSet() then
                        repeat
                            if ItemApplicationEntry2."Inbound Item Entry No." <> ItemApplicationEntry2."Item Ledger Entry No." then
                                ShippedQuantityNotReturned += ItemApplicationEntry2.Quantity;
                        until ItemApplicationEntry2.Next() = 0
                end
            until ItemApplicationEntry.Next() = 0
    end;

    local procedure UpdateSalesLineFromJobPlanLine(var TempSalesLine: Record "Sales Line"; JobContractEntryNo: Integer)
    var
        JobPlanningLine: Record "Job Planning Line";
    begin
        if JobContractEntryNo <> 0 then begin
            JobPlanningLine.SetCurrentKey("Job Contract Entry No.");
            JobPlanningLine.SetRange("Job Contract Entry No.", JobContractEntryNo);
            if JobPlanningLine.FindFirst() then begin
                TempSalesLine.Description := JobPlanningLine.Description;
                TempSalesLine."Work Type Code" := JobPlanningLine."Work Type Code";
                TempSalesLine."Unit Cost (LCY)" := JobPlanningLine."Unit Cost (LCY)";
                TempSalesLine.Validate("Location Code", JobPlanningLine."Location Code")
            end;
        end;
    end;

    local procedure CreateCorrInvItemChargeAssignLine(SalesHeader: Record "Sales Header"; SalesLine: Record "Sales Line"; SalesInvoiceLine: Record "Sales Invoice Line")
    var
        ItemChargeAssgntSales: Record "Item Charge Assignment (Sales)";
        Currency: Record Currency;
        TempItemLedgerEntry: Record "Item Ledger Entry" temporary;
        AssignItemChargeSales: Codeunit "Item Charge Assgnt. (Sales)";
        AssignableQty: Decimal;
        ItemChargeAssgntLineAmt: Decimal;
    begin
        InitCurrRoundingPrecision(SalesHeader, Currency);
        SalesLine.TestField(SalesLine.Type, SalesLine.Type::"Charge (Item)");
        SalesLine.TestField(SalesLine."No.");
        SalesLine.TestField(SalesLine.Quantity);
        ItemChargeAssgntSales.Reset();
        ItemChargeAssgntSales.SetRange("Document Type", SalesLine."Document Type");
        ItemChargeAssgntSales.SetRange("Document No.", SalesLine."Document No.");
        ItemChargeAssgntSales.SetRange("Document Line No.", SalesLine."Line No.");
        if not ItemChargeAssgntSales.FindLast() then begin
            if (SalesLine."Inv. Discount Amount" = 0) and
               (SalesLine."Line Discount Amount" = 0) and
               (not SalesHeader."Prices Including VAT")
            then
                ItemChargeAssgntLineAmt := SalesLine."Line Amount"
            else
                if SalesHeader."Prices Including VAT" then
                    ItemChargeAssgntLineAmt := Round((SalesLine."Line Amount" - SalesLine."Inv. Discount Amount") / (1 + SalesLine."VAT %" / 100), Currency."Amount Rounding Precision")
                else
                    ItemChargeAssgntLineAmt := SalesLine."Line Amount" - SalesLine."Inv. Discount Amount";

            GetSalesShipmentLinesForItemCharge(SalesInvoiceLine, TempItemLedgerEntry);
            TempItemLedgerEntry.Reset();
            if TempItemLedgerEntry.FindSet() then
                repeat
                    ItemChargeAssgntSales.Init();
                    ItemChargeAssgntSales."Document Type" := SalesLine."Document Type";
                    ItemChargeAssgntSales."Document No." := SalesLine."Document No.";
                    ItemChargeAssgntSales."Document Line No." := SalesLine."Line No.";
                    ItemChargeAssgntSales."Line No." := GetLastItemChargeSalesLineNo(SalesLine);
                    ItemChargeAssgntSales."Item Charge No." := SalesLine."No.";
                    ItemChargeAssgntSales."Applies-to Doc. Type" := ItemChargeAssgntSales."Applies-to Doc. Type"::Shipment;
                    ItemChargeAssgntSales."Applies-to Doc. No." := TempItemLedgerEntry."Document No.";
                    ItemChargeAssgntSales."Applies-to Doc. Line No." := TempItemLedgerEntry."Document Line No.";
                    ItemChargeAssgntSales."Item No." := TempItemLedgerEntry."Item No.";
                    ItemChargeAssgntSales.Description := GetItemDescription(TempItemLedgerEntry."Item No.", TempItemLedgerEntry.Description);
                    InsertItemChargeAssigment(ItemChargeAssgntSales);
                until TempItemLedgerEntry.Next() = 0;
        end;
        SalesLine.CalcFields("Qty. Assigned");
        AssignableQty := SalesLine."Qty. to Invoice" + SalesLine."Quantity Invoiced" - SalesLine."Qty. Assigned";
        AssignItemChargeSales.AssignItemCharges(SalesLine, AssignableQty, ItemChargeAssgntLineAmt, AssignItemChargeSales.AssignByAmountMenuText());
    end;

    local procedure GetItemDescription(ItemNo: Code[20]; Description: Text[100]): Text[100]
    var
        Item: Record Item;
    begin
        if Description <> '' then
            exit(Description);

        if Item.Get(ItemNo) then
            exit(Item.Description);

        exit('');
    end;

    local procedure InitCurrRoundingPrecision(SalesHeader: Record "Sales Header"; var Currency: Record Currency)
    begin
        if SalesHeader."Currency Code" = '' then
            Currency.InitRoundingPrecision()
        else begin
            SalesHeader.TestField("Currency Factor");
            Currency.Get(SalesHeader."Currency Code");
            Currency.TestField("Amount Rounding Precision");
        end;
    end;

    local procedure InsertItemChargeAssigment(ItemChargeAssgntSales: Record "Item Charge Assignment (Sales)")
    var
        AssignItemChargeSales: Codeunit "Item Charge Assgnt. (Sales)";
    begin

        AssignItemChargeSales.InsertItemChargeAssgnt(
          ItemChargeAssgntSales, ItemChargeAssgntSales."Applies-to Doc. Type", ItemChargeAssgntSales."Applies-to Doc. No.",
          ItemChargeAssgntSales."Applies-to Doc. Line No.", ItemChargeAssgntSales."Item No.", ItemChargeAssgntSales.Description, ItemChargeAssgntSales."Line No.");
    end;

    local procedure CreateItemTrackingLine(SalesLine: Record "Sales Line"; ItemLedgerEntryNo: Integer; ExactCostReverseMandatory: Boolean)
    var
        ReservationEntry: Record "Reservation Entry";
        ItemLedgerEntry: Record "Item Ledger Entry";
        LastEntryNo: Integer;
    begin
        if ItemLedgerEntry.Get(ItemLedgerEntryNo) then
            if ((ItemLedgerEntry."Lot No." <> '') or (ItemLedgerEntry."Serial No." <> '')) and
               (SalesLine."Quantity (Base)" <> 0)
            then begin
                ReservationEntry.LockTable();
                ReservationEntry.Reset();
                ReservationEntry.FindLast();
                LastEntryNo := ReservationEntry."Entry No." + 1;
                ReservationEntry.Init();
                ReservationEntry."Entry No." := LastEntryNo;
                ReservationEntry.Positive := true;
                ReservationEntry."Item No." := ItemLedgerEntry."Item No.";
                ReservationEntry."Location Code" := ItemLedgerEntry."Location Code";
                ReservationEntry."Quantity (Base)" := SalesLine."Quantity (Base)";
                ReservationEntry."Reservation Status" := ReservationEntry."Reservation Status"::Surplus;
                ReservationEntry."Creation Date" := WorkDate();
                ReservationEntry."Source Type" := 37;
                ReservationEntry."Source Subtype" := 3;
                ReservationEntry."Source ID" := SalesLine."Document No.";
                ReservationEntry."Source Ref. No." := SalesLine."Line No.";
                ReservationEntry."Expected Receipt Date" := WorkDate();
                ReservationEntry."Serial No." := ItemLedgerEntry."Serial No.";
                ReservationEntry."Created By" := UserId;
                ReservationEntry."Qty. per Unit of Measure" := SalesLine."Qty. per Unit of Measure";
                ReservationEntry.Quantity := SalesLine.Quantity;
                ReservationEntry."Warranty Date" := ItemLedgerEntry."Warranty Date";
                ReservationEntry."Expiration Date" := ItemLedgerEntry."Expiration Date";
                ReservationEntry."Qty. to Handle (Base)" := SalesLine."Quantity (Base)";
                ReservationEntry."Qty. to Invoice (Base)" := SalesLine."Quantity (Base)";
                ReservationEntry."Quantity Invoiced (Base)" := 0;
                ReservationEntry."Lot No." := ItemLedgerEntry."Lot No.";
                ReservationEntry."Variant Code" := ItemLedgerEntry."Variant Code";
                ReservationEntry."Appl.-from Item Entry" := ItemLedgerEntryNo;
                if ExactCostReverseMandatory then
                    ReservationEntry."Appl.-from Item Entry" := ItemLedgerEntry."Entry No.";
                ReservationEntry.UpdateItemTracking();
                ReservationEntry.Insert(true);
            end;
    end;

    local procedure UpdateCurrencyFactor(var TempSalesLine: Record "Sales Line"; var SalesHeader: Record "Sales Header")
    var
        SalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        if SalesHeader."Currency Code" = '' then
            exit;
        TempSalesLine.Reset();
        if TempSalesLine.FindFirst() then begin
            TempSalesLine.Reset();
            TempSalesLine.SetFilter("ITI Corr. Invoice Line No.", '<>%1', TempSalesLine."ITI Corr. Invoice Line No.");
            if TempSalesLine.IsEmpty and SalesInvoiceHeader.get(TempSalesLine."ITI Corr. Invoice No.") then begin
                SalesHeader.Validate("Currency Factor", SalesInvoiceHeader."Currency Factor");
                SalesHeader.Modify();
            end;
        end;
    end;

    local procedure GetLastItemChargeSalesLineNo(SalesLine: Record "Sales Line"): Integer
    var
        ItemChargeSales: Record "Item Charge Assignment (Sales)";
    begin
        ItemChargeSales.SetRange("Document Type", SalesLine."Document Type");
        ItemChargeSales.SetRange("Document No.", SalesLine."Document No.");
        ItemChargeSales.SetRange("Document Line No.", SalesLine."Line No.");
        if ItemChargeSales.FindLast() then
            exit(ItemChargeSales."Line No.");
        exit(0);
    end;

    local procedure GetSalesShipmentLinesForItemCharge(SalesInvoiceLine: Record "Sales Invoice Line"; var TempItemLedgerEntry: Record "Item Ledger Entry" temporary)
    var
        ItemLedgEntry: Record "Item Ledger Entry";
        ValueEntry: Record "Value Entry";
    begin
        TempItemLedgerEntry.Reset();
        TempItemLedgerEntry.DeleteAll();

        SalesInvoiceLine.FilterPstdDocLineValueEntries(ValueEntry);

        if ValueEntry.FindSet() then
            repeat
                ItemLedgEntry.Get(ValueEntry."Item Ledger Entry No.");
                if ItemLedgEntry."Document Type" = ItemLedgEntry."Document Type"::"Sales Shipment" then begin
                    TempItemLedgerEntry := ItemLedgEntry;
                    if TempItemLedgerEntry.Insert() then;
                end;
            until ValueEntry.Next() = 0;

    end;

    local procedure GetSalesCorrReason(SalesCorrectionReasonCode: Code[10])
    begin
        if (SalesCorrectionReason.Code <> SalesCorrectionReasonCode) and
           (SalesCorrectionReasonCode <> '')
        then
            SalesCorrectionReason.Get(SalesCorrectionReasonCode);
    end;

    [EventSubscriber(ObjectType::Codeunit, 80, 'OnAfterSalesCrMemoLineInsert', '', true, true)]
    local procedure OnAfterSalesCrMemoLineInsertITV(SalesLine: Record "Sales Line"; SalesCrMemoHeader: Record "Sales Cr.Memo Header")
    begin
        InsertPostedSalesCorrLine(SalesLine, SalesCrMemoHeader."No.");
    end;

    [EventSubscriber(ObjectType::Codeunit, 80, 'OnAfterFinalizePosting', '', true, true)]
    local procedure OnAfterFinalizePostingITV(var SalesHeader: Record "Sales Header")
    begin
        DeleteSalesCorrLines(SalesHeader);
    end;


    procedure InsertPostedSalesCorrLine(SalesLine: Record "Sales Line"; DocumentNo: Code[20])
    var
        SalesCorrLine: record "ITV Sales Corr. Line";
        PostedSalesCorrLine: record "ITV Posted Sales Corr. Line";
        PostedSalesCorrLine2: record "ITV Posted Sales Corr. Line";
    begin
        SalesCorrLine.SETRANGE("Document Type", SalesLine."Document Type");
        SalesCorrLine.SETRANGE("Document No.", SalesLine."Document No.");
        SalesCorrLine.SETRANGE("Correction Line No.", SalesLine."ITV Correction Line No.");
        IF SalesCorrLine.FINDSET THEN
            REPEAT
                PostedSalesCorrLine.INIT;
                PostedSalesCorrLine.TRANSFERFIELDS(SalesCorrLine);
                PostedSalesCorrLine."Document No." := DocumentNo;
                IF NOT PostedSalesCorrLine2.GET(PostedSalesCorrLine."Document No.",
                                                PostedSalesCorrLine."Correction Line No.",
                                                PostedSalesCorrLine."Before/ After correction")
                THEN
                    PostedSalesCorrLine.INSERT;
            UNTIL SalesCorrLine.NEXT = 0;
    end;


    procedure DeleteSalesCorrLines(SalesHeader: Record "Sales Header")
    var
        IsHandled: Boolean;
        SalesCorrLine: record "ITV Sales Corr. Line";
    begin

        IsHandled := FALSE;

        IF IsHandled THEN
            EXIT;

        SalesCorrLine.SETRANGE("Document Type", SalesHeader."Document Type");
        SalesCorrLine.SETRANGE("Document No.", SalesHeader."No.");
        IF NOT SalesCorrLine.ISEMPTY THEN
            SalesCorrLine.DELETEALL(false);
    end;

    procedure DeleteLineAfterChangeCorrReason(SalesHeader: Record "Sales Header"; FieldChangedDesc: Text[30])
    var
        SalesCorrLine: record "ITV Sales Corr. Line";
        Text92004: label 'When you change %1 field, all %2 will be deleted.\Are you sure you want to change %1';
        Text92010: label 'Action stopped.';
    begin
        SalesCorrLine.SETRANGE("Document Type", SalesHeader."Document Type");
        SalesCorrLine.SETRANGE("Document No.", SalesHeader."No.");
        // START/NAV2018PL/012
        // IF (NOT SalesLine.ISEMPTY) AND (NOT SalesCorrLine.ISEMPTY) THEN
        IF (NOT SalesCorrLine.ISEMPTY) THEN
            // STOP /NAV2018PL/012
            IF CONFIRM(Text92004, FALSE, FieldChangedDesc, SalesCorrLine.TABLECAPTION) THEN BEGIN
                DeleteSalesCorrLines(SalesHeader);
            END ELSE
                ERROR(Text92010);
    end;
}

