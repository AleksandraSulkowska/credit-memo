codeunit 50162 "ITVCreateSalesCorrLines"
{
    var
        LinesWillBeModifiedQst: Label 'There is one or more %1 created from %2. This lines will be modified.\Do you want to continue?', Comment = '%1 = SalesCorrLine TableCaption ; %2 = SalesInvoiceLine TableCaption';
        AlreadyCancelledErr: Label 'Posted Sales Invoice No. %1 is already cancelled.', Comment = '%1 = Posted Sales Invoice No.';
        NoSalesLineSelectedErr: Label 'No %1 selected.', Comment = '%1 = SalesInvoiceLine TableCaption';

    procedure CreateSalesCorrLines(SalesHeader: Record "Sales Header"; var SalesInvoiceLine: Record "Sales Invoice Line")
    var
        UpdateLines: Boolean;
    begin
        CheckIfAnySalesInvLinesSelected(SalesInvoiceLine);
        ConfirmSalesCorrLinesUpdate(SalesHeader, SalesInvoiceLine, UpdateLines);
        CheckSalesInvLines(SalesHeader, SalesInvoiceLine);
        InsertOrModifySalesCorrLines(SalesHeader, SalesInvoiceLine, UpdateLines);
    end;

    local procedure CheckIfAnySalesInvLinesSelected(var SalesInvoiceLine: Record "Sales Invoice Line")
    begin
        if SalesInvoiceLine.IsEmpty then
            Error(NoSalesLineSelectedErr, SalesInvoiceLine.TableCaption);
    end;

    local procedure ConfirmSalesCorrLinesUpdate(SalesHeader: Record "Sales Header"; var SalesInvoiceLine: Record "Sales Invoice Line"; var UpdateLines: Boolean)
    var
        SalesCorrLine: Record "ITV Sales Corr. Line";
        LinesExists: Boolean;
    begin
        LinesExists := false;
        UpdateLines := false;
        SalesInvoiceLine.FindSet();
        repeat
            SalesCorrLine.SetRange("Document Type", SalesHeader."Document Type");
            SalesCorrLine.SetRange("Document No.", SalesHeader."No.");
            SalesCorrLine.SetRange("Corr. Invoice No.", SalesInvoiceLine."Document No.");
            SalesCorrLine.SetRange("Corr. Invoice Line No.", SalesInvoiceLine."Line No.");
            if not SalesCorrLine.IsEmpty then
                LinesExists := true;
        until (SalesInvoiceLine.Next() = 0);

        if LinesExists then begin
            if not Confirm(LinesWillBeModifiedQst, false, SalesCorrLine.TableCaption, SalesInvoiceLine.TableCaption) then
                Error('');
            UpdateLines := true;
        end;
    end;

    local procedure CheckSalesInvLines(SalesHeader: Record "Sales Header"; var SalesInvoiceLine: Record "Sales Invoice Line")
    var
        SalesInvoiceHeader: Record "Sales Invoice Header";
        TempErrorMessage: Record "Error Message" temporary;
    begin
        SalesInvoiceLine.FindSet();
        repeat
            if SalesInvoiceLine."Document No." <> SalesInvoiceHeader."No." then begin
                SalesInvoiceHeader.Get(SalesInvoiceLine."Document No.");
                CheckSalesInvoiceHeader(SalesInvoiceHeader, SalesHeader, TempErrorMessage);
            end;
        until SalesInvoiceLine.Next() = 0;
        StopIfErrors(TempErrorMessage);
    end;

    local procedure CheckSalesInvoiceHeader(SalesInvoiceHeader: Record "Sales Invoice Header"; SalesHeader: Record "Sales Header"; var TempErrorMessage: Record "Error Message" temporary)
    begin
        SalesInvoiceHeader.CalcFields(Cancelled);
        if SalesInvoiceHeader.Cancelled then
            TempErrorMessage.LogSimpleMessage(TempErrorMessage."Message Type"::Error, StrSubstNo(AlreadyCancelledErr, SalesInvoiceHeader."No."));

        TempErrorMessage.LogIfNotEqualTo(SalesInvoiceHeader, SalesInvoiceHeader.FieldNo("Currency Code"), TempErrorMessage."Message Type"::Error, SalesHeader."Currency Code");
        TempErrorMessage.LogIfNotEqualTo(SalesInvoiceHeader, SalesInvoiceHeader.FieldNo("Prices Including VAT"), TempErrorMessage."Message Type"::Error, SalesHeader."Prices Including VAT");
    end;

    local procedure StopIfErrors(TempErrorMessage: Record "Error Message" temporary)
    begin
        if TempErrorMessage.HasErrors(true) then
            TempErrorMessage.ShowErrorMessages(true);
    end;

    local procedure InsertOrModifySalesCorrLines(SalesHeader: Record "Sales Header"; var SalesInvoiceLine: Record "Sales Invoice Line"; UpdateLines: Boolean)
    var
        SalesCorrLine: Record "ITV Sales Corr. Line";
        SalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        SalesInvoiceLine.FindSet();
        repeat
            if SalesInvoiceLine."Document No." <> SalesInvoiceHeader."No." then
                SalesInvoiceHeader.Get(SalesInvoiceLine."Document No.");
            SalesCorrLine.CopyFromSalesInvoiceLine(SalesInvoiceLine, SalesInvoiceHeader, SalesHeader);
            InsertOrModifySalesCorrLine(SalesCorrLine, UpdateLines);
        until SalesInvoiceLine.Next() = 0;
    end;

    local procedure InsertOrModifySalesCorrLine(SalesCorrLineNew: Record "ITV Sales Corr. Line"; UpdateLine: Boolean)
    var
        SalesCorrLineOld: Record "ITV Sales Corr. Line";
    begin
        if UpdateLine then begin
            SalesCorrLineOld.SetRange("Document Type", SalesCorrLineNew."Document Type");
            SalesCorrLineOld.SetRange("Document No.", SalesCorrLineNew."Document No.");
            SalesCorrLineOld.SetRange("Corr. Invoice No.", SalesCorrLineNew."Corr. Invoice No.");
            SalesCorrLineOld.SetRange("Corr. Invoice Line No.", SalesCorrLineNew."Corr. Invoice Line No.");
            if SalesCorrLineOld.FindFirst() then begin
                SalesCorrLineNew."Correction Line No." := SalesCorrLineOld."Correction Line No.";
                SalesCorrLineNew.Modify();
            end else
                SalesCorrLineNew.Insert(true);
        end else
            SalesCorrLineNew.Insert(true);
    end;
}

