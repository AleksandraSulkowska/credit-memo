enum 50160 "ITV Before/After Line Type"
{
    value(0; Before) { Caption = 'Before'; }
    value(1; After) { Caption = 'After'; }
}
