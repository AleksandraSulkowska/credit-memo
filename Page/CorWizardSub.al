page 50164 "ITV Sales Corr. Wizard Sub."
{
    Caption = 'Sales Corr. Wizard Subform';
    Editable = true;
    InsertAllowed = false;
    LinksAllowed = false;
    PageType = ListPart;
    RefreshOnActivate = true;
    ShowFilter = true;
    SourceTable = "ITV Sales Corr. Line";

    layout
    {
        area(content)
        {
            repeater(Control1170000)
            {
                ShowCaption = false;
                field("Corr. Invoice No."; Rec."Corr. Invoice No.")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = false;
                    Visible = false;
                }
                field("Corr. Invoice Line No."; Rec."Corr. Invoice Line No.")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = false;
                    Visible = false;
                }
                field("Correction Line No."; Rec."Correction Line No.")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = false;
                    Visible = false;
                }
                field("Before/ After correction"; Rec."Before/ After correction")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = false;
                }
                field("Consecutive No."; Rec."Consecutive No.")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = false;
                    Visible = false;
                }
                field(Type; Rec.Type)
                {
                    ApplicationArea = Basic, Suite;
                    Editable = false;
                }
                field("No."; Rec."No.")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = false;
                }
                field(Description; Rec.Description)
                {
                    ApplicationArea = Basic, Suite;
                    Editable = FieldEditable;
                }
                field("VAT Prod. Posting Group"; Rec."VAT Prod. Posting Group")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = FieldEditable;
                }
                field(Quantity; Rec.Quantity)
                {
                    ApplicationArea = Basic, Suite;
                    Editable = FieldEditable;
                }
                field("Unit of Measure Code"; Rec."Unit of Measure Code")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = false;
                }
                field("Unit of Measure"; Rec."Unit of Measure")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = false;
                    Visible = false;
                }
                field("Unit Price"; Rec."Unit Price")
                {
                    ApplicationArea = Basic, Suite;
                    BlankZero = true;
                    Editable = FieldEditable;
                }
                field("Line Amount"; Rec."Line Amount")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = FieldEditable;
                }
                field("Amount Including VAT"; Rec."Amount Including VAT")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = FieldEditable;
                    Visible = false;
                }
                field("VAT Base Amount"; Rec."VAT Base Amount")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = FieldEditable;
                    Visible = false;
                }
                field("Line Discount %"; Rec."Line Discount %")
                {
                    ApplicationArea = Basic, Suite;
                    BlankZero = true;
                    Editable = FieldEditable;
                }
                field("Line Discount Amount"; Rec."Line Discount Amount")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = FieldEditable;
                    Visible = false;
                }
                field("Inv. Discount Amount"; Rec."Inv. Discount Amount")
                {
                    ApplicationArea = Basic, Suite;
                    Editable = InvDiscountAmtEditable;
                }
                field("Inconsistent Line"; Rec."Inconsistent Line")
                {
                    ApplicationArea = Basic, Suite;
                    Visible = false;
                }
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetCurrRecord()
    begin
        SetControlsEditable();
    end;

    var
        SalesCorrReason: Record "ITV Sales Correction Reason";
        CurrentCorrReason: Boolean;
        [InDataSet]
        FieldEditable: Boolean;
        InvDiscountAmtEditable: Boolean;

    local procedure SetControlsEditable()
    begin
        FieldEditable := (Rec."Before/ After correction" = Rec."Before/ After correction"::After);
        GetCorrectionReason();
        InvDiscountAmtEditable := FieldEditable and Rec.InvoiceDiscAllowed(SalesCorrReason."Correction Type");
        CurrPage.Update(false);
    end;

    local procedure GetCorrectionReason()
    var
        SalesHeader: Record "Sales Header";
    begin
        if not CurrentCorrReason then
            if (SalesHeader.Get(Rec."Document Type", Rec."Document No.")) and
               (SalesCorrReason.Get(SalesHeader."ITI Correction Reason"))
            then
                CurrentCorrReason := true;
    end;
}

