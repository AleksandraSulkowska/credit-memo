page 50163 "ITV Sales Corr. Wizard"
{
    Caption = 'Sales Corr. Wizard';
    DeleteAllowed = false;
    Editable = true;
    InsertAllowed = false;
    LinksAllowed = false;
    PageType = Card;
    RefreshOnActivate = true;
    ShowFilter = false;
    SourceTable = "Sales Header";
    SourceTableView = SORTING("Document Type", "No.")
                      WHERE("Document Type" = FILTER("Credit Memo" | "Return Order"));

    layout
    {
        area(content)
        {
            group(Control1000000003)
            {
                ShowCaption = false;
                field("ITI Correction Reason"; Rec."ITI Correction Reason")
                {
                    ApplicationArea = Basic, Suite;
                    trigger OnValidate()
                    begin
                        CurrPage.Update();
                    end;
                }
                field("ITI Corr.Inv. Sal. Date Filter"; Rec."ITI Corr.Inv. Sal. Date Filter")
                {
                    ApplicationArea = Basic, Suite;
                }
            }
            part(SalesInvCorrWizardSubform; "ITV Sales Corr. Wizard Sub.")
            {
                ApplicationArea = Basic, Suite;
                Editable = true;
                SubPageLink = "Document Type" = FIELD("Document Type"),
                              "Document No." = FIELD("No.");
                SubPageView = SORTING("Document Type", "Document No.", "Correction Line No.", "Before/ After correction");
                UpdatePropagation = Both;
            }
        }
        //  area(factboxes)
        //{
        //  part(Total; "ITV Sales Corr. Wizard FactBox")
        // {
        //   ApplicationArea = Basic, Suite;
        // Caption = 'Total';
        //Provider = SalesInvCorrWizardSubform;
        //SubPageLink = "Document Type" = FIELD("Document Type"),
        //            "Document No." = FIELD("Document No."),
        //          "Line No." = FIELD("Line No."),
        //        "Before/ After correction" = FIELD("Before/ After correction");
        // }
        //}
    }

    actions
    {
        area(processing)
        {
            action(GetInvoiceLinesForCorr)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Get Invoice Lines for Corr.';
                Image = GetLines;
                Promoted = true;
                PromotedCategory = Process;

                trigger OnAction()
                begin
                    Rec.ITVGetInvoiceLinesForCorr();
                end;
            }
            action(CreateCrMemoLines)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Create Cr. Memo Lines';
                Image = CreateLinesFromJob;
                Promoted = true;
                PromotedCategory = Process;

                trigger OnAction()
                begin
                    Rec.ITVCreateCrMemoLines();
                end;
            }
        }
    }

    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        LookupOKOnPush();
    end;

    var
        IncosistienLinesQst: Label 'There is one or more inconsistent creator lines. Do you realy want to close this page?';

    procedure LookupOKOnPush()
    var
        SalesCorrLine: Record "ITV Sales Corr. Line";
    begin
        SalesCorrLine.SetRange("Document Type", Rec."Document Type");
        SalesCorrLine.SetRange("Document No.", Rec."No.");
        SalesCorrLine.SetRange("Inconsistent Line", true);
        if not SalesCorrLine.IsEmpty then
            if not Confirm(IncosistienLinesQst, false) then
                Error('');
    end;



}

