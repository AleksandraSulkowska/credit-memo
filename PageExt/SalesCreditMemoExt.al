pageextension 50160 "Sales Credit Memo" extends "Sales Credit Memo"
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        addfirst("F&unctions")
        {
            action("Sales Inv. Corr. Creator ITV")
            {
                CaptionML = ENU = 'Invoice Correction Creator', PLK = 'Kreator faktury korygującej';
                RunObject = page "ITV Sales Corr. Wizard";
                ApplicationArea = all;
                Ellipsis = true;
                RunPageView = sorting("Document Type", "No.");
                RunPageLink = "Document Type" = field("Document Type"), "No." = field("No.");
                RunPageOnRec = true;
                Image = SuggestGrid;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;


            }
        }
    }

    var
        myInt: Integer;
}