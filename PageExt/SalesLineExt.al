tableextension 50162 "ITV Sales Line Ext." extends "Sales Line"
{
    fields
    {

        field(50162; "ITV Correction Line No."; Integer)
        {
            Caption = 'Correction Line No.';
            DataClassification = CustomerContent;
            TableRelation = "ITV Sales Corr. Line"."Correction Line No." WHERE("Document Type" = FIELD("Document Type"), "Document No." = FIELD("Document No."));
            ValidateTableRelation = false;
        }

    }

    procedure ITVSalesLineRelatedSalesCorrLineExists(): Boolean
    var
        SalesCorrLine: Record "ITV Sales Corr. Line";
    begin
        SalesCorrLine.SetRange("Document Type", "Document Type");
        SalesCorrLine.SetRange("Document No.", "Document No.");
        SalesCorrLine.SetRange("Corr. Invoice No.", "ITI Corr. Invoice No.");
        SalesCorrLine.SetRange("Corr. Invoice Line No.", "ITI Corr. Invoice Line No.");
        exit(not SalesCorrLine.IsEmpty);
    end;
}

tableextension 50163 "ITV Posted Sales Line Ext." extends "Sales Invoice Line"
{
    fields
    {

        field(50162; "ITV Correction Line No."; Integer)
        {
            Caption = 'Correction Line No.';
            DataClassification = CustomerContent;
            TableRelation = "ITV Posted Sales Corr. Line"."Correction Line No." WHERE("Document No." = FIELD("Document No."));
            ValidateTableRelation = false;
        }

    }

}