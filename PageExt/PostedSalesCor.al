pageextension 50165 "Posted SC. Ext. ITV" extends "Posted Sales Credit Memo"
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        addlast("&Cr. Memo")
        {
            action("ITV Sales Corection Lines")
            {
                ApplicationArea = all;
                Caption = 'Sales Correction Lines';
                Image = TraceOppositeLine;
                RunObject = page "ITV Posted Sales Corr. Lines";
                RunPageLink = "Document No." = field("No.");
            }
        }
    }

    var
        myInt: Integer;
}