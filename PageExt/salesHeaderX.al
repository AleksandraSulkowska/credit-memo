tableextension 50161 "Sales Hed. Ext." extends "Sales Header"
{
    fields
    {
        modify("ITI Correction Reason")
        {
            trigger OnAfterValidate()
            var
                ITVCorMgt: codeunit ITVCreateSalesCrMemoLines;
            begin
                IF "ITI Correction Reason" <> xRec."ITI Correction Reason" THEN
                    ITVCorMgt.DeleteLineAfterChangeCorrReason(Rec, FIELDCAPTION("ITI Correction Reason"));
            end;
        }
    }





    procedure ITVGetInvoiceLinesForCorr()
    var
        GetSalesInvLinesforCorr: Codeunit ITVRecreateSalesCorrLines;
    begin
        GetSalesInvLinesforCorr.GetInvoiceLinesForCorr(Rec);
    end;



    procedure ITVCreateCrMemoLines()
    var
        CreateSalesCrMemoLines: Codeunit ITVCreateSalesCrMemoLines;

    begin
        CreateSalesCrMemoLines.CreateCrMemoLines(Rec);
    end;

    var
        myInt: Integer;
}